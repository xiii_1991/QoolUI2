#include "qoolfontmodel.h"

QOOL_NS_BEGIN

QoolFontModel::QoolFontModel(QObject* parent)
  : QAbstractListModel(parent) {
  m_families = QFontDatabase::families();
}

QVariant QoolFontModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  return section;
}

int QoolFontModel::rowCount(const QModelIndex &parent) const
{
  // For list models only the root node (an invalid parent) should return the list's size. For all
  // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
  if (parent.isValid())
    return 0;

  return m_families.count();
}

QVariant QoolFontModel::data(const QModelIndex &index, int role) const
{
  if (!index.isValid())
    return QVariant();

  if (role == Qt::DisplayRole)
    return m_families[index.row()];

  if (role == Qt::FontRole)
    return QFont(m_families[index.row()]);

  return QVariant();
}

QOOL_NS_END
