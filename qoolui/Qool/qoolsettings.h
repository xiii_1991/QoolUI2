#pragma once

#include "qool_global.h"

#include <QMutex>
#include <QObject>
#include <QQuickItem>
#include <QSettings>

QOOL_NS_BEGIN

class QOOLUI_EXPORT QoolSettings: public QObject {
  Q_OBJECT
  QML_ELEMENT
  QML_SINGLETON
  QML_UNCREATABLE("")

protected:
  QoolSettings();
  static QoolSettings* m_instance;
  QSettings* m_settings;

public:
  static QoolSettings* instance() {
    static QMutex mutex;
    if (! m_instance) {
      QMutexLocker locker(&mutex);
      if (! m_instance)
        m_instance = new QoolSettings();
    }
    return m_instance;
  }

  static QoolSettings* create(QQmlEngine*, QJSEngine*);

  Q_SLOT void setValue(const QString& key, const QVariant& value);
  Q_INVOKABLE QVariant value(const QString& key,
    const QVariant& defaultValue = QVariant()) const;

  Q_SIGNAL void valueChanged(QString key, QVariant value);
};

QOOL_NS_END
