import QtQuick
import QtQuick.Controls
import Qool
import Qool.Controls.Basic

Menu {
    id: root

    property bool showTitle: false
    property real itemWidth: 150
    property real itemHeight: 15
    property bool animationEnabled: QoolFn.defaultAnimationSwitchFor(this)

    background: QoolControlBasic {
        id: bg
        cutSize: root.showTitle ? QoolStyle.menuCutSize : 0
        showTitle: root.showTitle
        title: root.title
        implicitWidth: itemWidth + root.leftPadding + root.rightPadding
        implicitHeight: itemHeight
        spacing: 1
    }

    topPadding: bg.topPadding
    leftPadding: bg.leftPadding
    rightPadding: bg.rightPadding
    bottomPadding: bg.bottomPadding

    delegate: QoolMenuItem {}
}
