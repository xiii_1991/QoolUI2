import QtQuick
import QtQuick.Controls
import Qool

MenuSeparator {
    id: root

    property color color: QoolStyle.infoColor
    property color textColor: Qool.bwFromColor(root.color, true)
    property string text: qsTr("一条提示信息")

    topInset: 3
    bottomInset: topInset
    leftInset: 3
    rightInset: leftInset

    font.pixelSize: QoolStyle.tooltipTextFontPixelSize

    horizontalPadding: 8
    verticalPadding: 5

    QtObject {
        id: privateControl
        readonly property color fgColor: root.enabled ? root.textColor : QoolStyle.backgroundStrokeColor
        readonly property color bgColor: root.enabled ? root.color : QoolStyle.controlBackgroundColor
    }

    background: Rectangle {
        color: privateControl.bgColor
        border.width: 1
        border.color: privateControl.fgColor
        radius: 3
    }

    contentItem: Text {
        text: root.text
        color: privateControl.fgColor
        font: root.font
        horizontalAlignment: Text.AlignHCenter
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    }
}
