import QtQuick

HoverHandler {
    id: root

    property string text: qsTr("工具提示")
    property int openInterval: 2000
    property int closeInterval: 1000
    property real positionOffsetX: 0
    property real positionOffsetY: -5
    property real maxWidth: 300
    property int textFormat: Text.MarkdownText

    acceptedButtons: Qt.NoButton

    onHoveredChanged: {
        if (hovered)
            showing()
        else
            closing()
    }

    function showing() {
        if (text !== "") {
            let i = QoolToolTipWindow.openInterval
            if (i !== root.openInterval)
                QoolToolTipWindow.openInterval = root.openInterval
            QoolToolTipWindow.transientParent = Window.window
            QoolToolTipWindow.attachedObject = root.target
            QoolToolTipWindow.setTextFormat(root.textFormat)
            QoolToolTipWindow.text = root.text
            QoolToolTipWindow.maximumWidth = target.width
            QoolToolTipWindow.wantToShow()
        }
    }

    function closing() {
        let i = QoolToolTipWindow.closeInterval
        if (i !== root.closeInterval)
            QoolToolTipWindow.closeInterval = root.closeInterval
        QoolToolTipWindow.wantToClose()
    }
}
