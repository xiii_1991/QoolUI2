pragma Singleton

import QtQuick
import Qool

Window {
    id: root

    property string text
    property bool animationEnabled: QoolFn.defaultAnimationSwitchForWindow(
                                        transientParent)
    property color textColor: QoolStyle.backgroundColor
    property color bgColor: QoolStyle.tooltipColor
    property alias font: tipText.font
    property alias openInterval: openTimer.interval
    property alias closeInterval: closeTimer.interval
    property bool smoothPositioning: false

    property Item attachedObject
    property real offsetX
    property real offsetY

    flags: Qt.CustomizeWindowHint | Qt.FramelessWindowHint | Qt.ToolTip | Qt.NoDropShadowWindowHint
    color: "transparent"
    opacity: 0
    title: "QoolToolTipWindow"
    width: tipText.width
    height: tipText.height
    minimumWidth: 10
    minimumHeight: 8
    maximumWidth: 300

    function wantToShow() {
        privateControl.setWidth(root.maximumWidth)
        let bl_point = attachedObject.mapToGlobal(offsetX, offsetY)
        privateControl.setPosFromBottomLeftCornerPoint(bl_point)
        show()
        closeTimer.stop()
        if (root.opacity <= 0)
            openTimer.restart()
        else
            opacity = 1
    }

    function wantToClose() {
        openTimer.stop()
        if (root.opacity >= 1)
            closeTimer.restart()
    }

    function setTextFormat(f) {
        tipText.textFormat = f
    }

    QtObject {
        id: privateControl
        function setWidth(w) {
            if (w < root.minimumWidth)
                tipText.width = tipText.implicitWidth
            else
                tipText.width = Math.min(w, tipText.implicitWidth)
        }

        function setPosFromBottomLeftCornerPoint(p) {
            let xx = p.x
            let yy = p.y
            if (xx + root.width > Screen.desktopAvailableWidth)
                xx = Screen.desktopAvailableWidth - width
            if (yy + root.height < 0)
                yy = 0
            root.x = xx
            root.y = yy - root.height
        }
    }

    QoolCutCornerBox {
        id: bgBox
        color: root.bgColor
        strokeColor: root.textColor
        strokeWidth: 1
        cutSize: 4
        anchors.fill: parent
        z: -10
    }

    Text {
        id: tipText
        text: root.text
        color: root.textColor
        z: 20
        leftPadding: 8
        rightPadding: 8
        topPadding: 4
        bottomPadding: 4
        font.pixelSize: QoolStyle.tooltipTextFontPixelSize
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    }

    Timer {
        id: openTimer
        interval: 500
        onTriggered: root.opacity = 1
    }

    Timer {
        id: closeTimer
        interval: 2000
        onTriggered: root.opacity = 0
    }

    Behavior on opacity {
        enabled: animationEnabled
        NumberAnimation {
            duration: QoolStyle.controlTransitionDuration
        }
    }

    Behavior on textColor {
        enabled: animationEnabled
        NumberAnimation {
            duration: QoolStyle.controlTransitionDuration
        }
    }

    Behavior on color {
        enabled: animationEnabled
        NumberAnimation {
            duration: QoolStyle.controlTransitionDuration
        }
    }

    Behavior on x {
        enabled: animationEnabled && smoothPositioning
        NumberAnimation {
            duration: QoolStyle.controlTransitionDuration
        }
    }

    Behavior on y {
        enabled: animationEnabled && smoothPositioning
        NumberAnimation {
            duration: QoolStyle.controlTransitionDuration
        }
    }

    Behavior on width {
        enabled: animationEnabled && smoothPositioning
        NumberAnimation {
            duration: QoolStyle.controlTransitionDuration
        }
    }

    Behavior on height {
        enabled: animationEnabled && smoothPositioning
        NumberAnimation {
            duration: QoolStyle.controlTransitionDuration
        }
    }
}
