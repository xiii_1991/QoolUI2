import QtQuick
import QtQuick.Controls
import Qool.Controls.Basic
import Qool

QoolControlBasic {
    id: root

    property color highlightColor: QoolStyle.highlightColor

    QoolLockedCover {
        z: 89
    }

    QoolCutCornerBox {
        z: 89
        cutSize: root.cutSize
        anchors.fill: root
        color: "transparent"
        strokeWidth: root.strokeWidth
        strokeColor: QoolStyle.negativeColor
        visible: !parent.enabled
    }
}
