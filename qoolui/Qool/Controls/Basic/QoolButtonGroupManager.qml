pragma Singleton

import QtQuick
import Qool

QtObject {
    property var buttons: Object()
    property var groups: Object()

    function when_check_changed(item, group) {
        if (group === "") {
            console.debug("Empty group name, skipped.")
            return
        }

        if (!item.hasOwnProperty("checked")) {
            console.debug("Item is not a checked button, skipped.")
            return
        }

        let itemName = item.objectName
        if (itemName === "") {
            console.debug("Button has no uuid, generating one...")
            item.objectName = Qool.uuid()
            itemName = item.objectName
        }

        if (item.checked) {
            if (buttons.hasOwnProperty(group)
                    && buttons[group].objectName !== itemName) {
                console.log("Unchecking previous button: " + buttons[group].objectName)
                buttons[group].checked = false
            }
            buttons[group] = item
            groups[itemName] = group
            console.debug("New button:[" + itemName + "] for group: " + group)
        } else if (groups.hasOwnProperty(itemName)) {
            delete buttons[group]
            delete groups[itemName]
            console.debug(
                        "Removed button:[" + itemName + "] from group: " + group)
        }
    }
}
