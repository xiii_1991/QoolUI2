import QtQuick
import QtQuick.Controls
import Qool

QoolControlBasic {
    id: root

    property bool checkable: false
    property bool checked: false

    readonly property bool highlighted: checkable && checked

    property string groupName: ""

    readonly property bool down: mainMA.down
    readonly property bool containsMouse: mainMA.containsMouse

    property var checkingOperation: function () {
        root.checked = !root.checked
    }

    showTitle: false

    signal clicked(QtObject mouse)
    signal doubleClicked(QtObject mouse)
    signal rightClicked(QtObject mouse)

    QoolCutCornerBox {
        id: backgroundShape
        cutSize: root.cutSize
        width: root.width
        height: root.height
        visible: false
    }

    MouseArea {
        id: mainMA
        property bool down: false
        z: -99
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        anchors.fill: parent
        enabled: root.enabled
        containmentMask: backgroundShape
        hoverEnabled: true

        onPressed: down = true
        onReleased: down = false
        onClicked: ev => {
                       if (ev.button === Qt.RightButton)
                       root.rightClicked(ev)
                       else
                       root.clicked(ev)
                   }
        onDoubleClicked: ev => root.doubleClicked(ev)
    }

    Connections {
        enabled: root.checkable
        function onClicked(ev) {
            root.checkingOperation()
        }

        function onCheckedChanged() {
            QoolButtonGroupManager.when_check_changed(root, root.groupName)
        }
    }
}
