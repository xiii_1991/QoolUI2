import QtQuick
import QtQuick.Controls
import Qool

Control {
    id: root

    property string title: qsTr("控件标题")
    property bool showTitle: true
    property font titleFont
    titleFont.pixelSize: QoolStyle.controlTitleFontPixelSize

    property color bgColor: QoolStyle.controlBackgroundColor
    property color strokeColor: QoolStyle.backgroundStrokeColor
    property real strokeWidth: QoolStyle.controlStrokeWidth
    property real cutSize: QoolStyle.controlCutSize
    property real edgeSpace: QoolStyle.controlEdgeSpace

    property bool animationEnabled: QoolFn.defaultAnimationSwitchFor(this)

    property Component titleItem: Text {
        text: root.title
        font: root.titleFont
        horizontalAlignment: Text.AlighRight
        verticalAlignment: Text.AlignTop
        color: QoolStyle.textColor
        opacity: root.enabled ? 1 : 0.5
        Behavior on opacity {
            enabled: root.animationEnabled
            NumberAnimation {
                duration: QoolStyle.controlTransitionDuration
            }
        }
    }

    background: QoolCutCornerBox {
        id: bgBox
        color: root.bgColor
        strokeColor: root.strokeColor
        cutSize: root.cutSize
        strokeWidth: root.strokeWidth
    }

    containmentMask: bgBox

    topPadding: topInset + privateControl.topSpace
    bottomPadding: bottomInset + edgeSpace + strokeWidth
    leftPadding: leftInset + strokeWidth + edgeSpace
    rightPadding: rightInset + strokeWidth + edgeSpace

    Loader {
        id: titleLoader
        sourceComponent: root.titleItem
        active: root.showTitle
        anchors {
            top: parent.top
            right: parent.right
            topMargin: strokeWidth + edgeSpace
            //            leftMargin: Math.max(strokeWidth, cutSize) + edgeSpace
            //            left: parent.left
            rightMargin: strokeWidth + edgeSpace
        }
    }

    QtObject {
        id: privateControl
        readonly property real topSpace: Math.max(
                                             edgeSpace,
                                             (titleLoader.y + titleLoader.height))
    }
}
