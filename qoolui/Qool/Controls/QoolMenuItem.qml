import QtQuick
import QtQuick.Controls
import Qool

MenuItem {
    id: root

    property bool animationEnabled: QoolFn.defaultAnimationSwitchFor(this)

    font.pixelSize: QoolStyle.menuTextFontPixelSize

    background: Rectangle {
        color: QoolStyle.highlightColor
        height: root.enabled && root.highlighted ? 2 : 0
        implicitWidth: 200
        anchors.bottom: parent.bottom
        Behavior on height {
            enabled: root.animationEnabled
            NumberAnimation {
                duration: QoolStyle.controlTransitionDuration
            }
        }
    }

    arrow: Rectangle {
        x: parent.width - width
        anchors.verticalCenter: contentItem.verticalCenter
        opacity: 0.8
        visible: root.subMenu
        implicitHeight: contentItem.implicitHeight
        implicitWidth: 5
        color: QoolStyle.highlightColor
    }

    indicator: QoolSmallIndicator {
        animationEnabled: root.animationEnabled
        visible: root.checkable
        text: checked ? qsTr("已选") : qsTr("可选")
        color: checked ? QoolStyle.highlightColor : QoolStyle.textColor
        x: parent.width - width - 2
        anchors.verticalCenter: contentItem.verticalCenter
    }

    contentItem: Text {
        rightPadding: Math.max(root.arrow.width, root.indicator.width)
        text: root.text
        font: root.font
        color: root.highlighted ? QoolStyle.highlightColor : QoolStyle.textColor
        Behavior on color {
            enabled: root.animationEnabled
            ColorAnimation {
                duration: QoolStyle.controlTransitionDuration
            }
        }
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
        opacity: root.enabled ? 1 : 0.2
    }

    QoolHighlightCover {
        cutSize: QoolStyle.menuCutSize
        visible: (!subMenu) && down
        strokeColor: "transparent"
        strokeWidth: 0
    }

    QoolLockedIndicator {
        z: 30
    }
}
