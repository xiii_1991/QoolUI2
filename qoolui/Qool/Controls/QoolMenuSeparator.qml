import QtQuick
import QtQuick.Controls
import Qool

MenuSeparator {

    contentItem: Rectangle {
        implicitWidth: 200
        implicitHeight: 1
        color: QoolStyle.backgroundStrokeColor
    }
}
