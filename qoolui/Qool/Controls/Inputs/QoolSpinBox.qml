import QtQuick
import QtQuick.Controls
import Qool.Controls
import Qool

QoolControl {
    id: root

    property int numberFixMode: Qool.NumberGateCycleFix
    property bool editable: false
    property real from: 0
    property real to: 100
    property real stepSize: 1.5
    property real defaultValue: root.from
    property int decimals: 2
    property real value: defaultValue
    property color textColor: QoolStyle.textColor
    property var validator: function (x) {
        switch (root.numberFixMode) {
        case Qool.NumberGateContinuumFix:
            return numberGate.continuumFix(x)
        case Qool.NumberGateCycleFix:
            return numberGate.cycleFix(x)
        default:
            return numberGate.fix(x)
        }
    }

    property var valueFromText: function (x) {
        return parseFloat(x)
    }
    property var textFromValue: function (x) {
        let v = x.toFixed(root.decimals)
        if (v.endsWith("0"))
            return parseFloat(v).toString()
        return v
    }
    property string unit
    property color unitColor: textColor
    property Component unitItem: Text {
        text: root.unit
        font.pixelSize: QoolStyle.controlTitleFontPixelSize + 3
        color: root.unitColor
        opacity: root.enabled ? 1 : 0.5
        Behavior on opacity {
            enabled: root.animationEnabled
            NumberAnimation {
                duration: QoolStyle.controlTransitionDuration
            }
        }
    }

    readonly property bool editing: privateControl.editing

    QoolNumberGate {
        id: numberGate
        top: Math.max(root.from, root.to)
        bottom: Math.min(root.from, root.to)
    }

    function increase() {
        let new_value = root.value + root.stepSize
        root.value = root.validator(new_value)
    }

    function decrease() {
        let new_value = root.value - root.stepSize
        root.value = root.validator(new_value)
    }

    function edit() {
        privateControl.editing = true
        mainText.visible = false
        editor.visible = true
        editor.text = mainText.text
        editor.forceActiveFocus()
        editor.selectAll()
    }

    function reset() {
        root.value = root.defaultValue
    }

    QtObject {
        id: privateControl
        property bool editing: false
        function whenEditingFinished() {
            privateControl.editing = false
            let new_value = root.valueFromText(editor.text)
            if (!new_value)
                new_value = root.from
            root.value = root.validator(new_value)
            mainText.visible = true
            editor.visible = false
        }
    }

    contentItem: Item {
        Loader {
            id: unitItemLoader
            sourceComponent: root.unitItem
            anchors.right: parent.right
            anchors.bottom: parent.bottom
        }

        Text {
            id: mainText
            text: textFromValue(root.value)
            verticalAlignment: Text.AlignBottom
            horizontalAlignment: Text.AlignRight
            anchors {
                right: unitItemLoader.left
                bottom: parent.bottom
                rightMargin: 3
                top: parent.top
                topMargin: fromText.font.pixelSize
                left: parent.left
                leftMargin: 3
            }
            color: root.textColor
            font.pixelSize: QoolStyle.controlImportantTextSize
            fontSizeMode: Text.Fit
            visible: !root.editing
            opacity: root.enabled ? 1 : 0.5
            Behavior on opacity {
                enabled: root.animationEnabled
                NumberAnimation {
                    duration: QoolStyle.controlTransitionDuration
                }
            }
        }

        Text {
            id: fromText
            font.pixelSize: QoolStyle.controlDecorateTextSize
            font.bold: true
            text: root.textFromValue(root.from)
            anchors.top: parent.top
            anchors.left: parent.left
            color: root.textColor
            opacity: 0.5
        }

        Text {
            id: toText
            font: fromText.font
            text: root.textFromValue(root.to)
            anchors.top: parent.top
            anchors.right: parent.right
            color: root.textColor
            opacity: 0.5
        }

        Item {
            anchors.left: fromText.right
            anchors.right: toText.left
            anchors.margins: 3
            anchors.verticalCenter: fromText.verticalCenter
            height: 1
            Rectangle {
                color: root.textColor
                opacity: 0.5
                anchors.fill: parent
            }
            Rectangle {
                height: parent.height
                width: parent.width * Math.abs(
                           (root.value - root.from) / (root.to - root.from))
                color: root.highlightColor
                Behavior on width {
                    enabled: root.animationEnabled
                    NumberAnimation {
                        duration: QoolStyle.controlMovementDuration
                    }
                }
            }
        }

        MouseArea {
            id: mainMouseArea
            property real leftX: root.editable ? width / 3 : width / 2
            property real rightX: root.editable ? width * 2 / 3 : width / 2
            property int currentZone: mouseX <= leftX ? 1 : mouseX > rightX ? 2 : 0
            z: 10
            propagateComposedEvents: true
            visible: mainText.visible
            enabled: root.enabled
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton
            hoverEnabled: true
            cursorShape: currentZone === 0 ? Qt.IBeamCursor : Qt.ArrowCursor
            onClicked: switch (currentZone) {
                       case 1:
                           root.decrease()
                           break
                       case 2:
                           root.increase()
                           break
                       default:
                           root.edit()
                       }

            //            onEntered: root.entered()
            //            onExited: root.exited()
            QoolSmallIndicator {
                font.pixelSize: 10
                radius: 5
                text: switch (mainMouseArea.currentZone) {
                      case 1:
                          return "SUB -"
                      case 2:
                          return "ADD +"
                      default:
                          return qsTr("可编辑")
                      }
                anchors.bottom: parent.bottom
                x: 0
                color: switch (mainMouseArea.currentZone) {
                       case 1:
                           return QoolStyle.positiveColor
                       case 2:
                           return QoolStyle.negativeColor
                       default:
                           return root.highlightColor
                       }
                opacity: mainMouseArea.containsMouse ? 1 : 0
                animationEnabled: root.animationEnabled
            }
        }

        TextField {
            id: editor
            z: 20
            visible: false
            font: mainText.font
            selectByMouse: true
            verticalAlignment: TextEdit.AlignBottom
            horizontalAlignment: TextEdit.AlignRight
            anchors.right: unitItemLoader.left
            anchors.bottom: parent.bottom
            anchors.rightMargin: 3
            anchors.left: parent.left
            onEditingFinished: privateControl.whenEditingFinished()
            focus: visible
        }
    }
}
