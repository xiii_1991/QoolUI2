import QtQuick
import QtQuick.Controls
import Qool
import Qool.Controls
import Qool.Windows

QoolControl {
    id: root

    property color defaultValue
    property color value: defaultValue
    property color textColor: QoolStyle.textColor
    property int colorTextMode: Qool.NoColorText
    property bool showAlpha: false
    property Component colorTextItem: Loader {
        sourceComponent: switch (root.colorTextMode) {
                         case Qool.HexColorText:
                             return privateControl.hextCTItem
                         case Qool.RGBColorText:
                             return privateControl.rgbCTItem
                         case Qool.RGBFColorText:
                             return privateControl.rgbFloatCTItem
                         default:
                             return null
                         }
    }
    readonly property color bwColor: Qool.bwFromColor(root.value)

    function openColorDialog() {
        colorDialogLoader.active = true
        colorDialogLoader.item.setCurrentColor(root.value)
        colorDialogLoader.item.show()
    }

    function reset() {
        root.value = defaultValue
    }

    implicitWidth: 150
    implicitHeight: 65

    title: qsTr("选择颜色")
    contentItem: Item {
        Rectangle {
            id: colorBox
            color: root.enabled ? root.value : Qt.darker(root.value, 4)
            anchors {
                fill: parent
                margins: 3
            }
            radius: 5
            border.width: 1
            border.color: root.bwColor
            Loader {
                sourceComponent: colorTextItem
                anchors.centerIn: parent
            }
        }

        MouseArea {
            id: mainMA
            readonly property real splitX: root.editable ? width / 2 : width
            readonly property bool mouseInLeftZone: mouseX <= splitX
            hoverEnabled: true
            z: 10
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton
            cursorShape: Qt.PointingHandCursor
            enabled: root.enabled
            onClicked: openColorDialog()
            visible: colorBox.visible
            propagateComposedEvents: true
            QoolSmallIndicator {
                text: qsTr("选择颜色")
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                //                anchors.margins: 15
                color: QoolStyle.highlightColor
                opacity: mainMA.containsMouse ? 1 : 0
                animationEnabled: root.animationEnabled
            }
        }
    }

    Loader {
        id: colorDialogLoader
        active: false
        sourceComponent: Component {
            QoolColorDialog {
                visible: false
                transientParent: Window.window
                showAlpha: root.showAlpha
                animationEnabled: root.animationEnabled
            }
        }
        Connections {
            enabled: colorDialogLoader.active
            target: colorDialogLoader.item
            function onAccepted() {
                root.value = colorDialogLoader.item.currentColor
                releaseTimer.restart()
            }
            function onRejected() {
                releaseTimer.restart()
            }
        }
        Timer {
            id: releaseTimer
            interval: 50
            onTriggered: colorDialogLoader.active = false
        }
    }

    QtObject {
        id: privateControl

        property Component hextCTItem: Text {
            text: Qool.hexFromColor(root.value, root.showAlpha)
            font.pixelSize: QoolStyle.controlMainTextFontPixelSize
            color: root.bwColor
            opacity: 0.5
            horizontalAlignment: Qt.AlignRight
            verticalAlignment: Qt.AlignBottom
        }
        property Component rgbCTItem: Row {
            readonly property var values: Qool.rgbaStringFromColor(root.value)
            spacing: QoolStyle.controlDecorateTextSize * 2
            Text {
                text: 'R: ' + values[0]
                color: root.bwColor
                font.pixelSize: QoolStyle.controlDecorateTextSize
            }
            Text {
                text: 'G: ' + values[1]
                color: root.bwColor
                font.pixelSize: QoolStyle.controlDecorateTextSize
            }
            Text {
                text: 'B: ' + values[2]
                color: root.bwColor
                font.pixelSize: QoolStyle.controlDecorateTextSize
            }
            Text {
                visible: root.showAlpha
                text: 'A: ' + values[3]
                color: root.bwColor
                font.pixelSize: QoolStyle.controlDecorateTextSize
            }
        }
        property Component rgbFloatCTItem: Row {
            readonly property var values: Qool.rgbaStringFromColor(root.value)
            spacing: QoolStyle.controlDecorateTextSize * 2
            Text {
                text: 'R: ' + values[0]
                color: root.bwColor
                font.pixelSize: QoolStyle.controlDecorateTextSize
            }
            Text {
                text: 'G: ' + values[1]
                color: root.bwColor
                font.pixelSize: QoolStyle.controlDecorateTextSize
            }
            Text {
                text: 'B: ' + values[2]
                color: root.bwColor
                font.pixelSize: QoolStyle.controlDecorateTextSize
            }
            Text {
                visible: root.showAlpha
                text: 'A: ' + values[3]
                color: root.bwColor
                font.pixelSize: QoolStyle.controlDecorateTextSize
            }
        }
    }

    Behavior on value {
        enabled: root.animationEnabled
        ColorAnimation {
            duration: QoolStyle.controlMovementDuration
        }
    }
}
