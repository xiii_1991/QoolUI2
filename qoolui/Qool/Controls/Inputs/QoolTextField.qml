import QtQuick
import Qool.Controls
import Qool
import QtQuick.Controls

QoolControl {
    id: root
    property string defaultValue
    property string value: defaultValue

    property color textColor: QoolStyle.textColor
    Behavior on textColor {
        enabled: root.animationEnabled
        ColorAnimation {
            duration: QoolStyle.controlTransitionDuration
        }
    }
    property string emptyText: qsTr("(空)")
    property Component emptyTextItem: Text {
        text: emptyText
        font.pixelSize: QoolStyle.controlDecorateTextSize
        color: root.textColor
        opacity: root.value === "" ? 0.5 : 0
        Behavior on opacity {
            enabled: root.animationEnabled
            NumberAnimation {
                duration: QoolStyle.controlMovementDuration
            }
        }
    }

    property var valueFromText: function (x) {
        return x
    }
    property var textFromValue: function (x) {
        return x
    }

    property bool editable: true

    readonly property bool editing: privateControl.editing

    QtObject {
        id: privateControl
        property bool editing: false
        function whenEditingFinished() {
            privateControl.editing = false
            root.value = root.valueFromText(editor.text)
            mainText.visible = true
            editor.visible = false
        }
    }

    signal editingFinished

    function reset() {
        root.value = root.defaultValue
    }

    function edit() {
        privateControl.editing = true
        mainText.visible = false
        editor.visible = true
        editor.text = mainText.text
        editor.forceActiveFocus()
    }

    contentItem: Item {
        Text {
            id: mainText
            text: textFromValue(root.value)
            verticalAlignment: Text.AlignBottom
            horizontalAlignment: Text.AlignRight
            anchors.fill: parent
            color: root.textColor
            font.pixelSize: QoolStyle.controlMainTextFontPixelSize * 2
            fontSizeMode: Text.Fit
            visible: !root.editing
            opacity: root.enabled ? 1 : 0.5
            Behavior on opacity {
                enabled: root.animationEnabled
                NumberAnimation {
                    duration: QoolStyle.controlTransitionDuration
                }
            }
        }

        Loader {
            sourceComponent: root.emptyTextItem
            anchors.centerIn: parent
            visible: mainText.visible
        }

        MouseArea {
            id: mainMA
            z: 10
            visible: mainText.visible
            enabled: root.enabled
            anchors.fill: parent
            hoverEnabled: true
            propagateComposedEvents: true
            cursorShape: Qt.IBeamCursor
            onClicked: if (root.editable)
                           root.edit()
            QoolSmallIndicator {
                text: qsTr("可编辑")
                color: root.highlightColor
                anchors.left: parent.left
                anchors.bottom: parent.bottom
                animationEnabled: root.animationEnabled
                opacity: root.editable && mainMA.containsMouse ? 1 : 0
            }
        }

        TextField {
            id: editor
            z: 20
            visible: false
            font: mainText.font
            selectByMouse: true
            verticalAlignment: TextEdit.AlignBottom
            horizontalAlignment: TextEdit.AlignRight
            anchors.fill: parent
            onEditingFinished: privateControl.whenEditingFinished()
            focus: visible
        }
    }
}
