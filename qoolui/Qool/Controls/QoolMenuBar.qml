import QtQuick
import QtQuick.Controls
import Qool

MenuBar {
    id: root

    property color bgColor: QoolStyle.controlBackgroundColor
    property color textColor: QoolStyle.textColor

    property bool animationEnabled: QoolFn.defaultAnimationSwitchFor(this)

    delegate: MenuBarItem {
        id: menuBarItem
        font.pixelSize: QoolStyle.menuBarFontPixelSize
        hoverEnabled: true
        contentItem: Text {
            text: menuBarItem.text
            font: menuBarItem.font
            opacity: enabled ? 1 : 0.3
            color: menuBarItem.highlighted ? QoolStyle.highlightColor : root.textColor
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
            Behavior on color {
                enabled: root.animationEnabled
                ColorAnimation {
                    duration: QoolStyle.controlTransitionDuration
                }
            }
        }
        background: Rectangle {
            height: menuBarItem.highlighted ? 3 : 0
            width: parent.width - bgBox.strokeWidth * 2
            color: QoolStyle.highlightColor
            anchors.bottom: parent.bottom
            Behavior on height {
                enabled: root.animationEnabled
                NumberAnimation {
                    duration: QoolStyle.controlTransitionDuration
                }
            }
        }
    } //delegate

    background: QoolCutCornerBox {
        id: bgBox
        color: root.bgColor
        strokeWidth: 1
        strokeColor: root.textColor
        cutSize: QoolStyle.menuCutSize
    }
}
