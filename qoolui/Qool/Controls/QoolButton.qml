import QtQuick
import Qool
import Qool.Controls.Basic
import QtQuick.Shapes

QoolButtonBasic {
    id: root

    property color highlightColor: QoolStyle.highlightColor
    property string text: qsTr("酷酷的按钮")
    property color textColor: QoolStyle.textColor

    contentItem: Text {
        z: 10
        text: root.text
        horizontalAlignment: Qt.AlignHCenter
        verticalAlignment: Qt.AlignVCenter
        font.pixelSize: QoolStyle.controlMainTextFontPixelSize
        color: root.highlighted ? root.highlightColor : root.textColor
        opacity: root.enabled ? 1 : 0.5
        fontSizeMode: Text.Fit
        Behavior on opacity {
            enabled: root.animationEnabled
            NumberAnimation {
                duration: QoolStyle.controlTransitionDuration
            }
        }
        Behavior on color {
            enabled: root.animationEnabled
            ColorAnimation {
                duration: QoolStyle.controlTransitionDuration
            }
        }
    }

    QoolCutCornerBox {
        //附加的彩色边框
        cutSize: root.cutSize
        strokeColor: root.enabled ? (root.containsMouse
                                     || root.highlighted ? root.highlightColor : "transparent") : QoolStyle.negativeColor
        strokeWidth: root.strokeWidth
        color: "transparent"
        anchors.fill: parent
        z: 30
    }

    Rectangle {
        anchors.fill: parent
        anchors.margins: root.strokeWidth
        border.width: 0
        gradient: Gradient {
            orientation: Gradient.Vertical
            stops: [
                GradientStop {
                    position: 0.6
                    color: "transparent"
                },
                GradientStop {
                    position: 1
                    color: root.highlightColor
                }
            ]
        }
        opacity: root.containsMouse ? 0.2 : 0
        Behavior on opacity {
            enabled: root.animationEnabled
            NumberAnimation {
                duration: QoolStyle.controlTransitionDuration
            }
        }
        z: 5
    }

    QoolHighlightCover {
        highColor: root.highlightColor
        visible: root.down
        cutSize: root.cutSize
    }

    QoolLockedCover {
        z: 40
    }
}
