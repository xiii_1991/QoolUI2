#include "qoolsingleton.h"

#include <QCoreApplication>
#include <QUuid>

QOOL_NS_BEGIN

QoolSingleton::QoolSingleton()
  : QObject { qApp } {
}

QoolSingleton* QoolSingleton::create(QQmlEngine*, QJSEngine*) {
  return instance();
}

QColor QoolSingleton::bwFromColor(
  const QColor& color, bool preferBlack) {
  qreal v = color.valueF();
  if (preferBlack)
    return v < 0.2 ? Qt::white : Qt::black;
  return v < 0.75 ? Qt::white : Qt::black;
}

QoolSingleton* QoolSingleton::m_instance = nullptr;

QString QoolSingleton::hexFromColor(
  const QColor& color, bool withAlpha) {
  auto rr = QString("%1").arg(color.red(), 2, 16, QChar('0'));
  auto gg = QString("%1").arg(color.green(), 2, 16, QChar('0'));
  auto bb = QString("%1").arg(color.blue(), 2, 16, QChar('0'));
  if (withAlpha) {
    auto aa = QString("%1").arg(color.alpha(), 2, 16, QChar('0'));
    return QString("#%1%2%3%4").arg(aa, rr, gg, bb).toUpper();
  }
  return QString("#%1%2%3").arg(rr, gg, bb).toUpper();
}

QStringList QoolSingleton::rgbaStringFromColor(const QColor& color) {
  auto rr = QString::number(color.red());
  auto gg = QString::number(color.green());
  auto bb = QString::number(color.blue());
  auto aa = QString::number(color.alpha());
  return { rr, gg, bb, aa };
}

QStringList QoolSingleton::rgbaFloatStringFromColor(
  const QColor& color) {
  auto code = [](qreal x) -> QString {
    auto xx = QString::number(x, 'f', 3);
    if (x > 0 && xx.startsWith('0'))
      xx.remove(0, 1);
    return xx;
  };
  auto rr = code(color.redF());
  auto gg = code(color.greenF());
  auto bb = code(color.blueF());
  auto aa = code(color.alphaF());
  return { rr, gg, bb, aa };
}

QList<qreal> QoolSingleton::hsbaFromColor(const QColor& color) {
  auto c = color.toHsv();
  return { c.hsvHueF(), c.hsvSaturationF(), c.valueF(), c.alphaF() };
}

QList<qreal> QoolSingleton::hslaFromColor(const QColor& color) {
  auto c = color.toHsl();
  return { c.hslHueF(), c.hslSaturationF(), c.lightnessF(),
    c.alphaF() };
}

QList<qreal> QoolSingleton::cmykaFromColor(const QColor& color) {
  auto c = color.toCmyk();
  return { c.cyanF(), c.magentaF(), c.yellowF(), c.blackF(),
    c.alphaF() };
}

QColor QoolSingleton::colorFromCmyka(
  qreal c, qreal m, qreal y, qreal k, qreal a) {
  return QColor::fromCmykF(c, m, y, k, a);
}

qreal QoolSingleton::limitNumber(qreal x, qreal min, qreal max) {
  auto top = qMax(min, max);
  auto bottom = qMin(min, max);
  if (x < bottom)
    return bottom;
  if (x > top)
    return top;
  return x;
}

QStringList QoolSingleton::fontFamilies(bool allFonts) {
  auto fonts = QFontDatabase::families();
  qDebug() << tr("系统中共有字体%1个").arg(fonts.length());
  if (allFonts)
    return fonts;

  qDebug() << tr("自动过滤奇怪的字体族……");
  QStringList res;
  for (auto& x : fonts) {
    if (x.startsWith("."))
      continue;
    if (x.isEmpty())
      continue;
    res << x;
  }
  qDebug() << tr("过滤后共有字体%1个。").arg(res.length());
  return res;
}

QString QoolSingleton::uuid() {
  return QUuid::createUuid().toString(QUuid::WithoutBraces);
}

QOOL_NS_END
