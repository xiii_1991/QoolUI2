import QtQuick
import Qool.Windows.Basic
import "private/colordialogcomponents"

QoolDialogBasic {
    id: root

    readonly property color currentColor: mainForm.currentColor
    property alias showAlpha: mainForm.showAlpha

    height: mainForm.height + mainForm.y + 50
    width: 300
    resizable: false
    modality: Qt.WindowModal
    title: qsTr("QoolUI 颜色选择器")

    function setCurrentColor(c) {
        mainForm.setCurrentColor(c)
    }

    ColorSelectorForm {
        id: mainForm
        animationEnabled: root.animationEnabled
        y: 30
        x: 20
    }
}
