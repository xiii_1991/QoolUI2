import QtQuick
import Qool.Windows.Basic
import "private/fontdialogcomponents"

QoolDialogBasic {
    id: root

    readonly property string currentFamily: fontForm.currentFamily

    function setCurrentFamily(f) {
        fontForm.setCurrentFamily(f)
    }

    title: qsTr("字体选择")
    width: 400
    height: 600

    FontSelectionForm {
        id: fontForm
        anchors.fill: contentLoader
    }
}
