import QtQuick
import QtQuick.Window
import Qool.Windows.Basic
import Qool

QoolWindowBasic {
    id: root

    property alias closeButton: closeButton

    property Component toolBar: null
    property Component header: null
    property Component content: Item {}
    property Component footer: Item {
        height: resizeIndicator.height - spaceControl.bottomInset + root.strokeWidth
    }

    property alias toolBarLoader: toolBarLoader
    property alias headerLoader: headerLoader
    property alias contentLoader: contentLoader
    property alias footerLoader: footerLoader

    Loader {
        id: toolBarLoader
        x: root.cutSize + spaceControl.leftInset + root.strokeWidth
        y: root.strokeWidth + spaceControl.topInset
        height: Math.max(titleText.height, root.cutSize - root.strokeWidth)
        width: root.width - spaceControl.leftInset * 2 - spaceControl.rightInset
               - root.cutSize - root.strokeWidth * 2 - root.titleText.width
        sourceComponent: toolBar
    }

    Loader {
        id: headerLoader
        anchors.left: parent.left
        anchors.leftMargin: spaceControl.leftSpace
        anchors.right: parent.right
        anchors.rightMargin: spaceControl.rightSpace
        y: root.strokeWidth + spaceControl.topInset * 2 + Math.max(
               root.cutSize, titleText.height)
        sourceComponent: header
    }

    QtObject {
        id: spaceControl
        property real spacing: 5

        property real insets: 5
        property real topInset: spaceControl.insets
        property real bottomInset: spaceControl.insets
        property real leftInset: spaceControl.insets
        property real rightInset: spaceControl.insets

        property real margins: 0
        property real topMargin: spaceControl.margins
        property real bottomMargin: spaceControl.margins
        property real leftMargin: spaceControl.margins
        property real rightMargin: spaceControl.margins

        readonly property real topSpace: Math.max(
                                             root.cutSize + topInset + topMargin,
                                             titleText.y + titleText.height + spacing)
        readonly property real leftSpace: root.strokeWidth + leftInset + leftMargin
        readonly property real bottomSpace: root.strokeWidth
                                            + root.resizable ? Math.max(
                                                                   bottomInset,
                                                                   resizeIndicator.height) : bottomInset + bottomMargin
        readonly property real rightSpace: root.strokeWidth + rightInset + rightMargin
    }

    Loader {
        id: footerLoader
        anchors.left: parent.left
        anchors.leftMargin: spaceControl.leftSpace
        anchors.right: parent.right
        anchors.rightMargin: spaceControl.rightSpace + (root.resizable ? resizeIndicator.width : 0)
        anchors.bottom: parent.bottom
        anchors.bottomMargin: spaceControl.bottomInset + root.strokeWidth
        sourceComponent: footer
    }

    QoolWindowResizeIndicator {
        id: resizeIndicator
        visible: root.resizable
        z: -98
        color: root.strokeColor
        QoolDragMoveArea {
            id: resizeDragArea
            anchors.fill: parent
            property bool isFree: true
            cursorShape: Qt.PointingHandCursor
            property bool fallback: false
            Connections {
                function onPressed() {
                    let result = root.startSystemResize(
                            Qt.RightEdge | Qt.BottomEdge)
                    if (!result)
                        resizeDragArea.fallback = true
                }
                function onWantToMove(offsetX, offsetY) {
                    if (resizeDragArea.isFree && resizeDragArea.fallback) {
                        resizeDragArea.isFree = false
                        root.width = Math.max(root.width + offsetX,
                                              root.minimumWidth)
                        root.height = Math.max(root.height + offsetY,
                                               root.minimumHeight)
                        resizeDragArea.isFree = true
                    }
                }
            }
        } //QoolDragMoveArea
    }

    Loader {
        id: contentLoader
        anchors.left: parent.left
        anchors.leftMargin: spaceControl.leftSpace
        anchors.right: parent.right
        anchors.rightMargin: spaceControl.rightSpace
        anchors.top: headerLoader.bottom
        anchors.topMargin: spaceControl.topInset
        anchors.bottom: footerLoader.top
        anchors.bottomMargin: spaceControl.bottomInset
        sourceComponent: content
    }

    QoolWindowCloseButton {
        id: closeButton
        width: root.cutSize - 5
        strokeColor: root.strokeColor
        strokeWidth: root.strokeWidth
        color: QoolStyle.negativeColor
        hoveredColor: QoolStyle.highlightColor
        clickedColor: QoolStyle.yellowColor
        onClicked: root.close()
    }
}
