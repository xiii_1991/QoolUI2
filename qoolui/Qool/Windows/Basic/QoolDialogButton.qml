import QtQuick
import QtQuick.Controls
import Qool

AbstractButton {
    id: root

    property bool animationEnabled: QoolFn.defaultAnimationSwitchFor(this)
    property color color: "red"

    implicitHeight: mainText.implicitHeight + topPadding + bottomPadding
    implicitWidth: mainText.implicitWidth + Math.max(
                       leftPadding, bgBox.cutSize) + rightPadding

    containmentMask: bgBox

    background: QoolCutCornerBox {
        id: bgBox
        cutSize: 5
        strokeColor: root.color
        color: root.hovered ? root.color : "transparent"
        strokeWidth: 1
        Behavior on color {
            enabled: root.animationEnabled
            ColorAnimation {
                duration: QoolStyle.controlTransitionDuration
            }
        }
    }

    topPadding: 5
    bottomPadding: 5
    leftPadding: 10
    rightPadding: 10

    contentItem: Text {
        id: mainText
        text: root.text
        font.pixelSize: QoolStyle.controlMainTextFontPixelSize
        color: root.hovered ? Qool.bwFromColor(root.color, true) : root.color
        Behavior on color {
            enabled: root.animationEnabled
            ColorAnimation {
                duration: QoolStyle.controlTransitionDuration
            }
        }
    }

    QoolHighlightCover {
        cutSize: bgBox.cutSize
        visible: root.down
    }
}
