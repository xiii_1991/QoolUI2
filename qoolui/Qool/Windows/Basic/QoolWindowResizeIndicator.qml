import QtQuick

Canvas {
    id: root
    width: 20
    height: width
    anchors.right: parent.right
    anchors.bottom: parent.bottom

    property color color: "red"

    onPaint: {
        var c = getContext("2d")
        c.moveTo(width, 0)
        c.lineTo(width, height)
        c.lineTo(0, height)
        c.fillStyle = root.color
        c.fill()
    }
}
