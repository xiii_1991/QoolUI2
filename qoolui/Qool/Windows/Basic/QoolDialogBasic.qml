import QtQuick
import Qool

QoolWindowBasic {
    id: root

    property bool showButtons: true
    property Component content: Item {}
    property Component footer: privateControl.defaultButtonBar

    property alias contentLoader: contentLoader
    property alias footerLoader: footerLoader

    flags: Qt.CustomizeWindowHint | Qt.FramelessWindowHint | Qt.Dialog
    title: qsTr("对话框体")
    titleText.font.pixelSize: QoolStyle.dialogTitleFontPixelSize
    cutSize: QoolStyle.dialogCutSize
    visible: false
    resizable: false
    animationEnabled: false
    bgColor: QoolStyle.dialogBackgroundColor

    bgBoxLoader.sourceComponent: Qt.platform.os === "osx" ? privateControl.osxBackgroundItem : defaultBackgroundItem

    onVisibleChanged: if (visible)
                          requestActivate()

    signal accepted
    signal rejected
    signal canceled

    QtObject {
        id: privateControl
        property Component defaultButtonBar: Row {
            spacing: 8
            layoutDirection: Qt.RightToLeft
            QoolDialogButton {
                text: qsTr("好哒")
                onClicked: {
                    root.accepted()
                    root.close()
                }
                color: QoolStyle.positiveColor
            }
            QoolDialogButton {
                text: qsTr("算了吧")
                onClicked: {
                    root.rejected()
                    root.close()
                }
                color: QoolStyle.negativeColor
            }
        }
        property Component osxBackgroundItem: Rectangle {
            border.color: QoolStyle.backgroundStrokeColor
            border.width: root.strokeWidth
            radius: 10
            color: root.bgColor
        }
    } //privateControl

    QtObject {
        id: spaceControl
        property real spacing: 5

        property real insets: 5
        property real topInset: spaceControl.insets
        property real bottomInset: spaceControl.insets
        property real leftInset: spaceControl.insets
        property real rightInset: spaceControl.insets

        property real margins: 0
        property real topMargin: spaceControl.margins
        property real bottomMargin: spaceControl.margins
        property real leftMargin: spaceControl.margins
        property real rightMargin: spaceControl.margins

        readonly property real topSpace: Math.max(
                                             root.cutSize + topInset + topMargin,
                                             titleText.y + titleText.height + spacing)
        readonly property real leftSpace: root.strokeWidth + leftInset + leftMargin
        readonly property real bottomSpace: root.strokeWidth + bottomInset + bottomMargin
        readonly property real rightSpace: root.strokeWidth + rightInset + rightMargin
    }

    Loader {
        id: footerLoader
        sourceComponent: root.footer
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            bottomMargin: spaceControl.bottomSpace
            leftMargin: spaceControl.leftSpace
            rightMargin: spaceControl.rightSpace
        }
    }

    Loader {
        id: contentLoader
        sourceComponent: root.content
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: footerLoader.top
            topMargin: spaceControl.topSpace
            leftMargin: spaceControl.leftSpace
            rightMargin: spaceControl.rightSpace
            bottomMargin: spaceControl.spacing
        }
    }
}
