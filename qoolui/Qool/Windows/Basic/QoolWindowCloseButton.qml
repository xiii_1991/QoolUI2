import QtQuick
import QtQuick.Controls
import Qool

AbstractButton {
    id: root
    height: width
    property color color: "red"
    property color hoveredColor: "pink"
    property color clickedColor: "yellow"
    property alias strokeWidth: bgTriangle.strokeWidth
    property alias strokeColor: bgTriangle.strokeColor
    property bool animationEnabled: QoolFn.defaultAnimationSwitchFor(this)
    background: QoolCutCornerTriangle {
        id: bgTriangle
        color: down ? root.clickedColor : hovered ? root.hoveredColor : root.color
        strokeColor: QoolStyle.controlBackgroundColor2
        strokeWidth: 2
    }

    containmentMask: bgTriangle
}
