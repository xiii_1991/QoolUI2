import QtQuick
import QtQuick.Window
import QtQuick.Controls
import Qool

Window {
    id: root
    flags: Qt.CustomizeWindowHint | Qt.FramelessWindowHint | Qt.Window
    color: "transparent"
    minimumHeight: cutSize * 2
    minimumWidth: minimumHeight

    property alias bgBox: bgBoxLoader.item
    property alias bgBoxLoader: bgBoxLoader
    property alias titleText: titleText

    property real cutSize: QoolStyle.windowCutSize
    property color bgColor: QoolStyle.controlBackgroundColor2
    property color strokeColor: QoolStyle.backgroundStrokeColor
    property real strokeWidth: 1
    property bool resizable: true
    property bool animationEnabled: QoolFn.defaultAnimationSwitchForWindow(
                                        transientParent)

    readonly property Component defaultBackgroundItem: QoolCutCornerBox {
        z: -99
        cutSize: root.cutSize
        strokeColor: root.strokeColor
        color: root.bgColor
        strokeWidth: root.strokeWidth
    }

    Component.onCompleted: {
        root.x = (root.screen.desktopAvailableWidth - root.width) / 2
        root.y = (root.screen.desktopAvailableHeight - root.height) / 2
    }

    Loader {
        id: bgBoxLoader
        sourceComponent: defaultBackgroundItem
        anchors.fill: parent
    }

    QoolDragMoveArea {
        id: bgDragArea
        anchors.fill: parent
        containmentMask: bgBox
        property bool fallback: false
        onFallbackChanged: if (!fallback)
                               console.log("system window move falling back...")
        Connections {
            function onPressed() {
                let result = root.startSystemMove()
                if (!result)
                    bgDragArea.fallback = true
            }

            function onWantToMove(offsetX, offsetY) {
                if (bgDragArea.fallback) {
                    root.x = root.x + offsetX
                    root.y = root.y + offsetY
                }
            }
        }
    }

    Text {
        id: titleText
        text: root.title
        color: QoolStyle.foregroundColor
        font.pixelSize: QoolStyle.mainWindowFontPixelSize
        horizontalAlignment: Qt.AlignRight
        verticalAlignment: Qt.AlignVCenter
        anchors.right: parent.right
        anchors.rightMargin: spaceControl.rightSpace
        y: root.strokeWidth + spaceControl.topInset
    }
}
