import QtQuick
import QtQuick.Shapes
import Qool

Shape {
    id: root

    readonly property real hue: privateControl.hue
    readonly property real saturation: privateControl.sat
    property real brightness: 1
    readonly property color currentColor: Qt.hsva(hue, saturation,
                                                  brightness, 1)

    readonly property real radius: width / 2
    property string title: ""
    property color titleColor: currentColor
    property bool animationEnabled: QoolFn.defaultAnimationSwitchFor(this)

    height: width
    containsMode: Shape.FillContains

    function setColor(color) {
        let hsba = Qool.hsbaFromColor(color)
        privateControl.hue = hsba[0]
        privateControl.sat = hsba[1]
        indicator.x = indicator.targetPoint.x
        indicator.y = indicator.targetPoint.y
    }

    QtObject {
        id: privateControl
        property real hue
        property real sat
        function targetPositionFromValues() {
            let h = hue
            let s = sat
            let distance = root.radius * s
            let h_r = h
            if (h >= 0.75)
                h_r = 1 - h
            else if (h >= 0.5)
                h_r = h - 0.5
            else if (h >= 0.25)
                h_r = 0.5 - h
            let arc = h_r * Math.PI * 2

            let dx = Math.sin(arc) * distance
            let dy = Math.cos(arc) * distance

            if (h > 0.5)
                dx = dx * -1

            if (h > 0.75 || h < 0.25)
                dy = dy * -1

            let xx = root.radius + dx - indicator.width / 2
            let yy = root.radius + dy - indicator.height / 2
            return Qt.point(xx, yy)
        }

        function setupValuesFromIndicator() {
            let dx = indicator.x + indicator.width / 2 - root.radius
            let dy = indicator.y + indicator.height / 2 - root.radius
            let distance = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2))
            let s = distance / root.radius

            let arc = Math.atan(dx / dy)
            let h_ratio = Math.abs(arc / (Math.PI * 2))
            let h = 0
            if (dx >= 0 && dy < 0)
                h = h_ratio
            else if (dx >= 0 && dy >= 0)
                h = 0.5 - h_ratio
            else if (dx < 0 && dy >= 0)
                h = 0.5 + h_ratio
            else
                h = 1 - h_ratio
            privateControl.hue = h
            privateControl.sat = s
        }
    }

    ColorPickIndicator {
        id: indicator
        property point targetPoint: privateControl.targetPositionFromValues()
        x: targetPoint.x
        y: targetPoint.y
        color: root.currentColor
        z: 20

        function setCenter(xx, yy) {
            let dx = Math.abs(xx - root.radius)
            let dy = Math.abs(yy - root.radius)
            let distance = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2))
            if (distance > root.radius) {
                let ratio = root.radius / distance
                dx = dx * ratio
                dy = dy * ratio
            }
            if (xx < root.radius)
                dx = -1 * dx
            if (yy < root.radius)
                dy = -1 * dy
            indicator.x = root.radius + dx - indicator.width / 2
            indicator.y = root.radius + dy - indicator.height / 2
        }
    }

    MouseArea {
        id: mainMA
        anchors.fill: parent
        containmentMask: parent
        onPressed: event => {
                       indicator.setCenter(event.x, event.y)
                       privateControl.setupValuesFromIndicator()
                   }
        onPositionChanged: event => {
                               indicator.setCenter(event.x, event.y)
                               privateControl.setupValuesFromIndicator()
                           }
    }

    ShapePath {
        id: shapeRainbow
        startX: 0
        startY: root.radius
        strokeWidth: 1
        strokeColor: "black"
        PathArc {
            x: root.width
            y: root.radius
            radiusX: root.radius
            radiusY: radiusX
        }
        PathArc {
            x: 0
            y: root.radius
            radiusX: root.radius
            radiusY: radiusX
        }
        fillGradient: ConicalGradient {
            angle: 90
            centerX: root.radius
            centerY: root.radius
            GradientStop {
                position: 0
                color: Qt.hsva(1, 1, root.brightness, 1)
            }
            GradientStop {
                position: 0.1
                color: Qt.hsva(0.9, 1, root.brightness, 1)
            }
            GradientStop {
                position: 0.2
                color: Qt.hsva(0.8, 1, root.brightness, 1)
            }
            GradientStop {
                position: 0.3
                color: Qt.hsva(0.7, 1, root.brightness, 1)
            }
            GradientStop {
                position: 0.4
                color: Qt.hsva(0.6, 1, root.brightness, 1)
            }
            GradientStop {
                position: 0.5
                color: Qt.hsva(0.5, 1, root.brightness, 1)
            }
            GradientStop {
                position: 0.6
                color: Qt.hsva(0.4, 1, root.brightness, 1)
            }
            GradientStop {
                position: 0.7
                color: Qt.hsva(0.3, 1, root.brightness, 1)
            }
            GradientStop {
                position: 0.8
                color: Qt.hsva(0.2, 1, root.brightness, 1)
            }
            GradientStop {
                position: 0.9
                color: Qt.hsva(0.1, 1, root.brightness, 1)
            }
            GradientStop {
                position: 1
                color: Qt.hsva(0, 1, root.brightness, 1)
            }
        }
    } //shapeRainbow

    ShapePath {
        id: shapeLight
        startX: 0
        startY: root.radius
        strokeWidth: 1
        strokeColor: "transparent"
        PathArc {
            x: root.width
            y: root.radius
            radiusX: root.radius
            radiusY: radiusX
        }
        PathArc {
            x: 0
            y: root.radius
            radiusX: root.radius
            radiusY: radiusX
        }
        fillGradient: RadialGradient {
            centerX: root.radius
            centerY: centerX
            centerRadius: root.radius
            focalX: centerX
            focalY: centerY
            GradientStop {
                position: 0
                color: Qt.hsva(0, 0, root.brightness, 1)
            }
            GradientStop {
                position: 1
                color: "transparent"
            }
        }
    } //shapelight

    HoverHandler {
        id: hoverHandler
    }

    QoolSmallIndicator {
        text: qsTr("色相&饱和度")
        opacity: hoverHandler.hovered ? 100 : 0
        x: 0
        y: 0 - height
        color: root.titleColor
        Behavior on opacity {
            enabled: root.animationEnabled
            NumberAnimation {
                duration: QoolStyle.controlTransitionDuration
            }
        }
    }
}
