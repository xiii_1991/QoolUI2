import QtQuick
import Qool.Controls
import Qool

Item {
    id: root

    readonly property real hue: wheel.hue
    readonly property real saturation: wheel.saturation
    readonly property real brightness: brightnessSlider.brightness
    readonly property real alpha: alphaSlider.alpha
    readonly property color currentColor: Qt.hsva(hue, saturation,
                                                  brightness, alpha)

    property bool showAlpha: true
    property bool animationEnabled: true

    implicitHeight: mainColumn.implicitHeight
    implicitWidth: 200

    function setCurrentColor(c) {
        wheel.setColor(c)
        let hsba = Qool.hsbaFromColor(c)
        brightnessSlider.value = 1 - hsba[2]
        alphaSlider.value = 1 - hsba[3]
    }

    Column {
        id: mainColumn
        spacing: 15
        ColorWheel {
            id: wheel
            brightness: brightnessSlider.brightness
            width: 200
            height: 200
            animationEnabled: root.animationEnabled
        }
        ColorBrightnessSlider {
            id: brightnessSlider
            headColor: wheel.currentColor
            height: 25
            animationEnabled: root.animationEnabled
        }
        ColorAlphaSlider {
            id: alphaSlider
            headColor: wheel.currentColor
            visible: showAlpha
            height: 25
            animationEnabled: root.animationEnabled
        }
    }
}
