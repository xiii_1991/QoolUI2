import QtQuick
import Qool

ColorSlider {
    id: root

    property color headColor: "black"
    readonly property color tailColor: {
        let hsba = Qool.hsbaFromColor(headColor)
        return Qt.hsva(hsba[0], hsba[1], 0, 1)
    }

    property real brightness: 1 - value

    function setBrightness(x) {
        setValue(1 - x)
    }

    title: qsTr("明亮度")
    titleColor: QoolStyle.highlightColor

    indicatorColor: {
        let hsba = Qool.hsbaFromColor(headColor)
        return Qt.hsva(hsba[0], hsba[1], brightness, 1)
    }

    background: Rectangle {
        radius: height / 2
        border {
            color: "black"
            width: 1
        }
        gradient: Gradient {
            orientation: Gradient.Horizontal
            stops: [
                GradientStop {
                    position: 0
                    color: root.headColor
                },
                GradientStop {
                    position: 1
                    color: root.tailColor
                }
            ]
        }
    }
}
