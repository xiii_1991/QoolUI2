import QtQuick
import Qool

ColorSlider {
    id: root

    property color headColor: "black"
    readonly property color tailColor: "white"

    property real alpha: 1 - value

    indicatorColor: {
        let hsba = Qool.hsbaFromColor(headColor)
        return Qt.hsva(hsba[0], hsba[1], hsba[2], alpha)
    }

    function setAlpha(x) {
        root.value = 1 - x
    }

    title: qsTr("不透明度")
    titleColor: "white"

    background: Rectangle {
        radius: height / 2
        border {
            color: "black"
            width: 1
        }
        gradient: Gradient {
            orientation: Gradient.Horizontal
            stops: [
                GradientStop {
                    position: 0
                    color: root.headColor
                },
                GradientStop {
                    position: 1
                    color: root.tailColor
                }
            ]
        }
    }
}
