import QtQuick
import QtQuick.Controls
import Qool

Item {
    id: root

    property color currentColor: QoolSettings.value(privateControl.settingKey,
                                                    Qt.white)
    property bool isEmpty: true
    property bool animationEnabled: QoolFn.defaultAnimationSwitchFor(this)
    property int currentSlot: 0
    property string text: privateControl.slotName

    width: 45
    height: 25

    containmentMask: bgBox

    signal leftClicked
    signal rightClicked

    function saveColor(c) {
        root.currentColor = c
        QoolSettings.setValue(privateControl.settingKey, c)
    }

    signal wantToSetColor(color c)

    HoverHandler {
        id: hoverHandler
    }

    TapHandler {
        id: leftHandler
        acceptedButtons: Qt.LeftButton
        onTapped: root.leftClicked()
    }

    TapHandler {
        id: rightHandler
        acceptedButtons: Qt.RightButton
        onTapped: root.rightClicked()
    }

    QtObject {
        id: privateControl
        readonly property color strokeColor: Qool.bwFromColor(root.currentColor)
        readonly property string slotNumber: {
            return "0".repeat(2).concat(root.currentSlot).slice(-2)
        }
        readonly property string slotName: "SLOT#" + slotNumber
        readonly property string settingKey: "color_palette/slot_" + slotNumber
    }

    QoolCutCornerBox {
        id: bgBox
        cutSize: 3
        anchors.fill: parent
        z: -10
        strokeWidth: 1
        strokeColor: privateControl.strokeColor
        color: root.currentColor
        Text {
            text: root.text
            color: privateControl.strokeColor
            font.pixelSize: 5
            anchors {
                top: parent.top
                right: parent.right
                topMargin: 2
                rightMargin: 2
            }
        }
        opacity: hoverHandler.hovered ? 1 : 0
        Behavior on opacity {
            enabled: root.animationEnabled
            NumberAnimation {
                duration: QoolStyle.controlTransitionDuration
            }
        }
    }

    Rectangle {
        border.width: 1
        border.color: hoverHandler.hovered ? "transparent" : privateControl.strokeColor
        color: root.currentColor
        radius: 3
        anchors {
            fill: parent
            topMargin: 9
            leftMargin: 2
            rightMargin: 2
            bottomMargin: 2
        }
        Behavior on color {
            enabled: root.animationEnabled
            ColorAnimation {
                duration: QoolStyle.controlTransitionDuration
            }
        }
    }

    QoolHighlightCover {
        visible: leftHandler.pressed
        z: 10
        cutSize: bgBox.cutSize
    }

    QoolHighlightCover {
        visible: rightHandler.pressed
        z: 10
        cutSize: bgBox.cutSize
        highColor: QoolStyle.positiveColor
    }
}
