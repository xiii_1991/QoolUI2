import QtQuick
import QtQuick.Shapes
import Qool

Column {
    id: root

    property bool showAlpha: true
    property bool animationEnabled: true

    readonly property real lightness: privateControl.lightness
    readonly property real alpha: alphaSlider.alpha
    readonly property color currentColor: Qt.rgba(lightness, lightness,
                                                  lightness, alpha)

    property real radius: 5

    spacing: 15

    function setCurrentColor(c) {
        let a = Qool.hsbaFromColor(c)
        privateControl.lightness = a[2]
    }

    QtObject {
        id: privateControl
        property real lightness: 0
        function targetPosisionFrom(a) {
            let x = mainShape.width * lightness - indicator.width / 2
            let y = mainShape.height / 2 - indicator.width / 2
            return Qt.point(x, y)
        }
    }

    Shape {
        id: mainShape
        implicitWidth: 200
        implicitHeight: 150
        containsMode: Shape.BoundingRectContains
        function setColor(c) {
            indicator.x = indicator.targetPoint.x
            indicator.y = indicator.targetPoint.y
        }
        ColorPickIndicator {
            id: indicator
            property point targetPoint: privateControl.targetPosisionFrom(
                                            root.lightness)
            x: targetPoint.x
            y: targetPoint.y
            z: 20
            color: root.currentColor
            function setCenter(xx, yy) {
                let xxx = Qool.limitNumber(xx, 0, mainShape.width)
                let yyy = Qool.limitNumber(yy, 0, mainShape.height)
                indicator.x = xxx - indicator.width / 2
                indicator.y = yyy - indicator.width / 2
            }
            function updateLightness() {
                let a = (x + width / 2) / mainShape.width
                privateControl.lightness = a
            }
        }

        MouseArea {
            id: mainMA
            anchors.fill: parent
            z: 50
            onPressed: ev => {
                           indicator.setCenter(ev.x, ev.y)
                           indicator.updateLightness()
                       }
            onPositionChanged: ev => {
                                   indicator.setCenter(ev.x, ev.y)
                                   indicator.updateLightness()
                               }
        }

        ShapePath {
            id: blackBack
            strokeWidth: 1
            strokeColor: "black"
            fillGradient: LinearGradient {
                x1: 0
                y1: 0
                x2: mainShape.width
                y2: 0
                GradientStop {
                    position: 0
                    color: "black"
                }
                GradientStop {
                    position: 1
                    color: "white"
                }
            }

            startX: 0
            startY: root.radius
            PathArc {
                x: root.radius
                y: 0
                radiusX: root.radius
                radiusY: root.radius
            }
            PathLine {
                x: mainShape.width - root.radius
                y: 0
            }
            PathArc {
                x: mainShape.width
                y: root.radius
                radiusX: root.radius
                radiusY: root.radius
            }
            PathLine {
                x: mainShape.width
                y: mainShape.height - root.radius
            }
            PathArc {
                x: mainShape.width - root.radius
                y: mainShape.height
                radiusX: root.radius
                radiusY: root.radius
            }
            PathLine {
                x: root.radius
                y: mainShape.height
            }
            PathArc {
                x: 0
                y: mainShape.height - root.radius
                radiusX: root.radius
                radiusY: root.radius
            }
            PathLine {
                x: 0
                y: root.radius
            }
        } //shapePath

        HoverHandler {
            id: hoverHandler
        }

        QoolSmallIndicator {
            text: qsTr("灰度")
            opacity: hoverHandler.hovered ? 100 : 0
            x: 0
            y: 0 - height
            color: "white"
            Behavior on opacity {
                enabled: root.animationEnabled
                NumberAnimation {
                    duration: QoolStyle.controlTransitionDuration
                }
            }
        }
    } //mainShape

    ColorAlphaSlider {
        id: alphaSlider
        animationEnabled: root.animationEnabled
        visible: root.showAlpha
    }
}
