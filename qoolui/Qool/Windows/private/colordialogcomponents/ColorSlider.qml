import QtQuick
import Qool

Item {
    id: root
    property real value: 0
    property bool animationEnabled: QoolFn.defaultAnimationSwitchFor(this)
    property color indicatorColor: "red"
    property color strokeColor: "white"

    property string title: ""
    property color titleColor: indicatorColor

    property Component background: Rectangle {
        color: "black"
        radius: height / 2
    }

    implicitHeight: 25
    implicitWidth: 200

    QoolNumberGate {
        id: numGate
        top: 1
        bottom: 0
    }

    QtObject {
        id: privateControl
        function xFromValue(v) {
            return (root.width - indicator.width) * v
        }
        function valueFromX(x) {
            return x / (root.width - indicator.width)
        }
    }

    ColorPickIndicator {
        id: indicator
        color: root.indicatorColor
        x: (root.width - indicator.width) * root.value
        width: height
        height: parent.height
        radius: height / 2
        border.color: root.strokeColor
        z: 10
    }

    MouseArea {
        id: mainMA
        anchors.fill: parent
        onPressed: ev => {
                       root.value = numGate.fix(ev.x / root.width)
                   }
        onPositionChanged: ev => {
                               root.value = numGate.fix(ev.x / root.width)
                           }
    }

    Loader {
        id: bgLoader
        z: -10
        sourceComponent: root.background
        anchors {
            fill: parent
            margins: indicator.border.width
        }
    }

    HoverHandler {
        id: hoverHandler
        enabled: root.title !== ""
    }

    QoolSmallIndicator {
        text: root.title
        opacity: hoverHandler.hovered ? 100 : 0
        x: 0
        y: 0 - height
        color: root.titleColor
        Behavior on opacity {
            enabled: root.animationEnabled
            NumberAnimation {
                duration: QoolStyle.controlTransitionDuration
            }
        }
    }
}
