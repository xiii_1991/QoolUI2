import QtQuick
import QtQuick.Controls
import Qool

Item {
    id: root

    property bool showAlpha: true
    property bool animationEnabled: QoolFn.defaultAnimationSwitchFor(this)
    readonly property color currentColor: pickerLoader.item ? pickerLoader.item.currentColor : "white"

    implicitWidth: headRow.width
    height: mainLayout.height

    function setCurrentColor(c) {
        pickerLoader.item.setCurrentColor(c)
    }

    QtObject {
        id: privateControl
        property color colorCache
        function changePickerTo(cc, tt) {
            if (!cc)
                return
            privateControl.colorCache = pickerLoader.item.currentColor
            pickerLoader.source = ""
            pickerLoader.source = tt
            pickerLoader.item.setCurrentColor(privateControl.colorCache)
        }
    }

    ButtonGroup {
        id: modeButtons
    }

    Column {
        id: mainLayout
        spacing: 15
        Row {
            id: headRow
            spacing: 10
            ColorMonitor {
                id: monitor
                width: 150
                height: parent.height
                currentColor: root.currentColor
            }

            Column {
                id: selector
                width: 50
                spacing: 6
                ColorPickerModeButton {
                    text: qsTr("HSB模式")
                    ButtonGroup.group: modeButtons
                    checked: true
                    color: QoolStyle.highlightColor
                    onCheckedChanged: privateControl.changePickerTo(
                                          checked, "ColorHSBPicker.qml")
                }
                ColorPickerModeButton {
                    text: qsTr("HSL模式")
                    ButtonGroup.group: modeButtons
                    color: QoolStyle.positiveColor
                    onCheckedChanged: privateControl.changePickerTo(
                                          checked, "ColorHSLPicker.qml")
                }
                ColorPickerModeButton {
                    text: qsTr("CMYK模式")
                    ButtonGroup.group: modeButtons
                    color: QoolStyle.infoColor
                    onCheckedChanged: privateControl.changePickerTo(
                                          checked, "ColorCMYKPicker.qml")
                }
                ColorPickerModeButton {
                    text: qsTr("灰度模式")
                    ButtonGroup.group: modeButtons
                    color: QoolStyle.tooltipColor
                    onCheckedChanged: privateControl.changePickerTo(
                                          checked, "ColorGreyscalePicker.qml")
                }
            }
        }
        Loader {
            id: pickerLoader
            source: "ColorHSBPicker.qml"
            Binding {
                target: pickerLoader.item
                property: "animationEnabled"
                value: root.animationEnabled
                when: source !== ""
            }
            Binding {
                target: pickerLoader.item
                property: "showAlpha"
                value: root.showAlpha
                when: source !== ""
            }
            x: 20
        }
        Grid {
            id: colorSlots
            columns: 5
            rows: 3
            columnSpacing: 6
            rowSpacing: 6
            Repeater {
                model: 15
                ColorSlotButton {
                    animationEnabled: root.animationEnabled
                    currentSlot: index
                    onRightClicked: {
                        saveColor(root.currentColor)
                    }
                    onLeftClicked: pickerLoader.item.setCurrentColor(
                                       currentColor)
                }
            }
        }
    } //mainColumn
}
