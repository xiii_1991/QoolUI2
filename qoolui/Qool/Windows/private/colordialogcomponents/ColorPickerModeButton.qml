import QtQuick
import QtQuick.Controls
import Qool

AbstractButton {
    id: root
    property color color

    checkable: true
    width: 100
    height: 20
    background: Rectangle {
        border {
            width: 2
            color: root.color
        }
        color: root.down || root.checked ? root.color : "transparent"
        radius: height / 2
    }

    contentItem: Text {
        id: mainText
        text: root.text
        font.pixelSize: QoolStyle.controlMainTextFontPixelSize
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        color: root.down || root.checked ? Qool.bwFromColor(root.color,
                                                            true) : root.color
    }
}
