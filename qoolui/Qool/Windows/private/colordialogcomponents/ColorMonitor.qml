import QtQuick
import QtQuick.Shapes
import Qool

Shape {
    id: root
    property color currentColor
    readonly property color currentSolidColor: {
        let hsba = Qool.hsbaFromColor(currentColor)
        return Qt.hsva(hsba[0], hsba[1], hsba[2], 1)
    }
    property color strokeColor: "black"
    property real radius: 5

    implicitWidth: 200
    implicitHeight: 150

    ShapePath {
        id: blackBack
        strokeWidth: 1
        strokeColor: root.strokeColor
        fillColor: "black"
        startX: 0
        startY: root.radius
        PathArc {
            x: root.radius
            y: 0
            radiusX: root.radius
            radiusY: root.radius
        }
        PathLine {
            x: root.width - root.radius
            y: 0
        }
        PathArc {
            x: root.width
            y: root.radius
            radiusX: root.radius
            radiusY: root.radius
        }
        PathLine {
            x: root.width
            y: root.height - root.radius
        }
        PathArc {
            x: root.width - root.radius
            y: root.height
            radiusX: root.radius
            radiusY: root.radius
        }
        PathLine {
            x: root.radius
            y: root.height
        }
        PathArc {
            x: 0
            y: root.height - root.radius
            radiusX: root.radius
            radiusY: root.radius
        }
        PathLine {
            x: 0
            y: root.radius
        }
    } //blackBack

    ShapePath {
        id: whiteBack
        strokeWidth: 1
        strokeColor: "transparent"
        fillColor: "white"
        startX: 0
        startY: root.radius
        PathArc {
            x: root.radius
            y: 0
            radiusX: root.radius
            radiusY: root.radius
        }
        PathLine {
            x: root.width - root.radius
            y: 0
        }
        PathArc {
            x: root.width
            y: root.radius
            radiusX: root.radius
            radiusY: root.radius
        }
        PathLine {
            x: root.width
            y: root.height / 2
        }
        PathLine {
            x: 0
            y: root.height / 2
        }
        PathLine {
            x: 0
            y: root.radius
        }
    } //blackBack

    ShapePath {
        id: solidBox
        strokeWidth: 1
        strokeColor: "transparent"
        fillColor: root.currentSolidColor
        startX: 0
        startY: root.radius
        PathArc {
            x: root.radius
            y: 0
            radiusX: root.radius
            radiusY: root.radius
        }
        PathLine {
            x: root.width / 2
            y: 0
        }
        PathLine {
            x: root.width / 2
            y: root.height
        }
        PathLine {
            x: root.radius
            y: root.height
        }
        PathArc {
            x: 0
            y: root.height - root.radius
            radiusX: root.radius
            radiusY: root.radius
        }
        PathLine {
            x: 0
            y: root.radius
        }
    } //solidBox

    ShapePath {
        id: colorBox
        strokeWidth: 1
        strokeColor: "transparent"
        fillColor: root.currentColor
        startX: root.width / 2
        startY: 0
        PathLine {
            x: root.width - root.radius
            y: 0
        }
        PathArc {
            x: root.width
            y: root.radius
            radiusX: root.radius
            radiusY: root.radius
        }
        PathLine {
            x: root.width
            y: root.height - root.radius
        }
        PathArc {
            x: root.width - root.radius
            y: root.height
            radiusX: root.radius
            radiusY: root.radius
        }
        PathLine {
            x: root.width / 2
            y: root.height
        }
        PathLine {
            x: root.width / 2
            y: 0
        }
    } //colorBox
}
