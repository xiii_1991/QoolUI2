import QtQuick

Rectangle {
    width: 25
    height: width
    radius: width / 2
    border.width: 4
    border.color: "white"
}
