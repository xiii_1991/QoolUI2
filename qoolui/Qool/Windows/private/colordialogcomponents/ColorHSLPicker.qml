import QtQuick
import Qool

Item {
    id: root
    property bool showAlpha: true
    property bool animationEnabled: true

    readonly property real hue: slider.hue
    readonly property real saturation: box.saturation
    readonly property real lightness: box.lightness
    readonly property real alpha: alphaSlider.alpha
    readonly property color currentColor: Qt.hsla(hue, saturation,
                                                  lightness, alpha)

    implicitHeight: mainColumn.implicitHeight
    implicitWidth: 200

    function setCurrentColor(c) {
        box.setColor(c)
        let hsva = Qool.hsbaFromColor(c)
        slider.value = hsva[0]
        alphaSlider.setAlpha(hsva[3])
    }

    Column {
        id: mainColumn
        spacing: 15
        ColorHSLBox {
            id: box
            width: 200
            height: 150
            hue: slider.hue
            animationEnabled: root.animationEnabled
        }
        ColorHueSlider {
            id: slider
            height: 25
            animationEnabled: root.animationEnabled
        }
        ColorAlphaSlider {
            id: alphaSlider
            visible: showAlpha
            height: 25
            animationEnabled: root.animationEnabled
        } //alphaSlider
    }
}
