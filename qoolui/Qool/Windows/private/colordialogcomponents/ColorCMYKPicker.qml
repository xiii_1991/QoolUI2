import QtQuick
import Qool

Item {
    id: root
    property bool showAlpha: true
    property bool animationEnabled: true
    readonly property real cyan: cyanSlider.value
    readonly property real magenta: magentaSlider.value
    readonly property real yellow: yellowSlider.value
    readonly property real black: blackSlider.value
    readonly property real alpha: alphaSlider.alpha
    readonly property color currentColor: Qool.colorFromCmyka(cyan,
                                                              magenta, yellow,
                                                              black, alpha)

    implicitWidth: 200
    implicitHeight: mainColumn.implicitHeight

    function setCurrentColor(c) {
        let w = Qool.cmykaFromColor(c)
        cyanSlider.value = w[0]
        magentaSlider.value = w[1]
        yellowSlider.value = w[2]
        blackSlider.value = w[3]
        alphaSlider.setAlpha(w[4])
    }

    Column {
        id: mainColumn
        spacing: 15
        ColorSlider {
            id: cyanSlider
            title: qsTr("CMYK:C")
            indicatorColor: "cyan"
            animationEnabled: root.animationEnabled
            background: Rectangle {
                radius: height / 2
                border {
                    color: "black"
                    width: 1
                }
                gradient: Gradient {
                    orientation: Gradient.Horizontal
                    stops: [
                        GradientStop {
                            position: 0
                            color: "white"
                        },
                        GradientStop {
                            position: 1
                            color: cyanSlider.indicatorColor
                        }
                    ]
                }
            }
        } //cyanSlider
        ColorSlider {
            id: magentaSlider
            indicatorColor: "magenta"
            title: qsTr("CMYK:M")
            animationEnabled: root.animationEnabled
            background: Rectangle {
                radius: height / 2
                border {
                    color: "black"
                    width: 1
                }
                gradient: Gradient {
                    orientation: Gradient.Horizontal
                    stops: [
                        GradientStop {
                            position: 0
                            color: "white"
                        },
                        GradientStop {
                            position: 1
                            color: magentaSlider.indicatorColor
                        }
                    ]
                }
            }
        } //magentaSlider
        ColorSlider {
            id: yellowSlider
            indicatorColor: "yellow"
            title: qsTr("CMYK:Y")
            animationEnabled: root.animationEnabled
            background: Rectangle {
                radius: height / 2
                border {
                    color: "black"
                    width: 1
                }
                gradient: Gradient {
                    orientation: Gradient.Horizontal
                    stops: [
                        GradientStop {
                            position: 0
                            color: "white"
                        },
                        GradientStop {
                            position: 1
                            color: yellowSlider.indicatorColor
                        }
                    ]
                }
            }
        } //yellowSlider
        ColorSlider {
            id: blackSlider
            indicatorColor: "black"
            title: qsTr("CMYK:K")
            titleColor: "white"
            animationEnabled: root.animationEnabled
            background: Rectangle {
                radius: height / 2
                border {
                    color: "black"
                    width: 1
                }
                gradient: Gradient {
                    orientation: Gradient.Horizontal
                    stops: [
                        GradientStop {
                            position: 0
                            color: "white"
                        },
                        GradientStop {
                            position: 1
                            color: blackSlider.indicatorColor
                        }
                    ]
                }
            }
        } //blackSlider
        ColorAlphaSlider {
            id: alphaSlider
            visible: root.showAlpha
            animationEnabled: root.animationEnabled
        }
    }
}
