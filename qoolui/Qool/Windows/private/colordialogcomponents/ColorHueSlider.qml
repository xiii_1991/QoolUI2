import QtQuick
import Qool

ColorSlider {
    id: root

    readonly property real hue: value

    indicatorColor: Qt.hsva(hue, 1, 1, 1)

    title: qsTr("色相")
    titleColor: QoolStyle.warningColor

    background: Rectangle {
        radius: height / 2
        border {
            color: "black"
            width: 1
        }
        gradient: Gradient {
            orientation: Gradient.Horizontal
            stops: [
                GradientStop {
                    position: 0
                    color: Qt.hsva(0, 1, 1, 1)
                },
                GradientStop {
                    position: 0.1
                    color: Qt.hsva(0.1, 1, 1, 1)
                },
                GradientStop {
                    position: 0.2
                    color: Qt.hsva(0.2, 1, 1, 1)
                },
                GradientStop {
                    position: 0.3
                    color: Qt.hsva(0.3, 1, 1, 1)
                },
                GradientStop {
                    position: 0.4
                    color: Qt.hsva(0.4, 1, 1, 1)
                },
                GradientStop {
                    position: 0.5
                    color: Qt.hsva(0.5, 1, 1, 1)
                },
                GradientStop {
                    position: 0.6
                    color: Qt.hsva(0.6, 1, 1, 1)
                },
                GradientStop {
                    position: 0.7
                    color: Qt.hsva(0.7, 1, 1, 1)
                },
                GradientStop {
                    position: 0.8
                    color: Qt.hsva(0.8, 1, 1, 1)
                },
                GradientStop {
                    position: 0.9
                    color: Qt.hsva(0.9, 1, 1, 1)
                },
                GradientStop {
                    position: 1
                    color: Qt.hsva(1, 1, 1, 1)
                }
            ]
        }
    }
}
