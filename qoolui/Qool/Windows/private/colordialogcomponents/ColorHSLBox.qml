import QtQuick
import QtQuick.Shapes
import Qool

Shape {
    id: root

    property bool animationEnabled: true
    property string title: ""
    property color titleColor: currentColor

    property real hue: 0
    readonly property real saturation: privateControl.saturation
    readonly property real lightness: privateControl.lightness
    readonly property color currentColor: Qt.hsla(hue, saturation, lightness, 1)

    property real radius: 5

    containsMode: Shape.BoundingRectContains

    function setColor(c) {
        let hsla = Qool.hslaFromColor(c)
        privateControl.saturation = hsla[1]
        privateControl.lightness = hsla[2]
        indicator.x = indicator.targetPoint.x
        indicator.y = indicator.targetPoint.y
    }

    QtObject {
        id: privateControl
        property real saturation: 1
        property real lightness: 0.5
        function targetPositionFrom(sat, light) {
            let x = root.width * sat - indicator.width / 2
            let y = root.height * light - indicator.height / 2
            return Qt.point(x, y)
        }
    }

    ColorPickIndicator {
        id: indicator
        property point targetPoint: privateControl.targetPositionFrom(
                                        root.saturation, root.lightness)
        x: targetPoint.x
        y: targetPoint.y
        z: 20
        color: root.currentColor
        function setCenter(xx, yy) {
            let xxx = Qool.limitNumber(xx, 0, root.width)
            let yyy = Qool.limitNumber(yy, 0, root.height)
            indicator.x = xxx - indicator.width / 2
            indicator.y = yyy - indicator.width / 2
        }
        function updateSaturation() {
            let sat = (x + width / 2) / root.width
            privateControl.saturation = sat
        }
        function updateLightness() {
            let light = (y + height / 2) / root.height
            privateControl.lightness = 1 - light
        }
    }

    MouseArea {
        id: mainMA
        anchors.fill: parent
        z: 50
        onPressed: ev => {
                       indicator.setCenter(ev.x, ev.y)
                       indicator.updateSaturation()
                       indicator.updateLightness()
                   }
        onPositionChanged: ev => {
                               indicator.setCenter(ev.x, ev.y)
                               indicator.updateSaturation()
                               indicator.updateLightness()
                           }
    }

    ShapePath {
        id: satBack
        strokeWidth: 1
        strokeColor: "black"
        fillGradient: LinearGradient {
            x1: 0
            y1: 0
            x2: root.width
            y2: 0
            GradientStop {
                position: 0
                color: Qt.hsla(root.hue, 0, 0.5, 1)
            }
            GradientStop {
                position: 1
                color: Qt.hsla(root.hue, 1, 0.5, 1)
            }
        }
        startX: 0
        startY: root.radius
        PathArc {
            x: root.radius
            y: 0
            radiusX: root.radius
            radiusY: root.radius
        }
        PathLine {
            x: root.width - root.radius
            y: 0
        }
        PathArc {
            x: root.width
            y: root.radius
            radiusX: root.radius
            radiusY: root.radius
        }
        PathLine {
            x: root.width
            y: root.height - root.radius
        }
        PathArc {
            x: root.width - root.radius
            y: root.height
            radiusX: root.radius
            radiusY: root.radius
        }
        PathLine {
            x: root.radius
            y: root.height
        }
        PathArc {
            x: 0
            y: root.height - root.radius
            radiusX: root.radius
            radiusY: root.radius
        }
        PathLine {
            x: 0
            y: root.radius
        }
    } //satBack

    ShapePath {
        id: lightBack
        startX: root.radius
        startY: 0
        strokeColor: "transparent"
        fillGradient: LinearGradient {
            x1: 0
            y1: 0
            x2: 0
            y2: root.height
            GradientStop {
                position: 0
                color: "white"
            }
            GradientStop {
                position: 0.5
                color: "transparent"
            }
            GradientStop {
                position: 1
                color: "black"
            }
        }

        PathArc {
            x: root.radius
            y: 0
            radiusX: root.radius
            radiusY: root.radius
        }
        PathLine {
            x: root.width - root.radius
            y: 0
        }
        PathArc {
            x: root.width
            y: root.radius
            radiusX: root.radius
            radiusY: root.radius
        }
        PathLine {
            x: root.width
            y: root.height - root.radius
        }
        PathArc {
            x: root.width - root.radius
            y: root.height
            radiusX: root.radius
            radiusY: root.radius
        }
        PathLine {
            x: root.radius
            y: root.height
        }
        PathArc {
            x: 0
            y: root.height - root.radius
            radiusX: root.radius
            radiusY: root.radius
        }
        PathLine {
            x: 0
            y: root.radius
        }
    }
    HoverHandler {
        id: hoverHandler
    }

    QoolSmallIndicator {
        text: qsTr("饱和度&亮度")
        opacity: hoverHandler.hovered ? 100 : 0
        x: 0
        y: 0 - height
        color: root.titleColor
        Behavior on opacity {
            enabled: root.animationEnabled
            NumberAnimation {
                duration: QoolStyle.controlTransitionDuration
            }
        }
    }
}
