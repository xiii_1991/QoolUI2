import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import Qool
import Qool.Controls

ScrollView {
    id: root
    property string currentFamily: ""

    signal familySelected(string family)

    function setCurrentFamily(f) {
        root.currentFamily = f
        privateControl.buttons[f].checked = true
    }

    function jumpTo(name) {
        if (!privateControl.buttons.hasOwnProperty(name)) {
            console.debug("No such family name as: " + name + ", skipping...")
            return
        }
        let pos = privateControl.buttons[name].y - root.height / 2
        let ratio = pos / column.implicitHeight
        let real_ratio = Qool.limitNumber(ratio, 0, 1)
        ScrollBar.vertical.position = ratio
        console.debug("Set scroll bar position to " + real_ratio)
    }

    QtObject {
        id: privateControl
        property var buttons: Object()
    }

    ColumnLayout {
        id: column
        width: root.width - root.ScrollBar.vertical.width
        spacing: 4
        Repeater {
            model: Qool.fontFamilies()
            delegate: FontPreviewer {
                id: previewer
                family: modelData
                Layout.alignment: Qt.AlignHCenter
                onFamilySelected: f => {
                                      root.familySelected(f)
                                  }
                Component.onCompleted: {
                    if (root.currentFamily === family)
                        checked = true
                    privateControl.buttons[family] = previewer
                }
            }
        }
    } //Column
}
