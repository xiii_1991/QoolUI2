import QtQuick
import Qool.Controls
import Qool

Item {
    id: root

    property alias currentFamily: panel.currentFamily
    property bool animationEnabled: QoolFn.defaultAnimationSwitchFor(this)

    function setCurrentFamily(f) {
        panel.setCurrentFamily(f)
    }

    FontPanel {
        id: panel
        height: parent.height - currentFamilyButton.height - 5
        width: parent.width
        onFamilySelected: f => {
                              QoolSettings.setValue("defaultFontFamily", f)
                              root.currentFamily = f
                          }
    }

    QoolButton {
        id: currentFamilyButton
        height: 45
        cutSize: 5
        width: parent.width
        y: parent.height - height
        contentItem: Text {
            text: root.currentFamily
            font.pixelSize: 18
            font.family: root.currentFamily
            color: QoolStyle.textColor
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
        onClicked: {
            panel.jumpTo(root.currentFamily)
        }
    }
}
