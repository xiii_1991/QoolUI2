import QtQuick
import Qool
import Qool.Controls.Basic

Item {
    id: root
    property string family
    property color textColor: QoolStyle.textColor
    property real textPixelSize: 18

    property bool checked: false

    property bool animationEnabled: QoolFn.defaultAnimationSwitchFor(this)

    implicitWidth: Math.max(plainText.contentWidth, previewLoader.width)
    implicitHeight: Math.max(plainText.contentHeight, previewLoader.height)

    signal familySelected(string family)

    onCheckedChanged: {
        QoolButtonGroupManager.when_check_changed(root,
                                                  "_font_select_button_group_")
    }

    HoverHandler {
        id: hoverHandler
    }

    TapHandler {
        onTapped: {
            root.checked = true
            root.familySelected(root.family)
        }
    }

    QtObject {
        id: privateControl
        property color displayColor: root.checked ? QoolStyle.highlightColor : root.textColor
        Behavior on displayColor {
            enabled: root.animationEnabled
            ColorAnimation {
                duration: QoolStyle.controlTransitionDuration
            }
        }
    }

    Rectangle {
        color: "transparent"
        border.color: privateControl.displayColor
        height: 1
        width: hoverHandler.hovered ? parent.width : 0
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        Behavior on width {
            enabled: root.animationEnabled
            NumberAnimation {
                duration: QoolStyle.controlTransitionDuration
            }
        }
    }

    Text {
        id: plainText
        text: root.family
        color: privateControl.displayColor
        font.pixelSize: root.textPixelSize
        anchors.centerIn: parent
        visible: previewLoader.status != Loader.Ready
    }

    Loader {
        id: previewLoader
        sourceComponent: Text {
            text: root.family
            color: privateControl.displayColor
            font.pixelSize: root.textPixelSize
            font.family: root.family
        }
        anchors.centerIn: parent
    }
}
