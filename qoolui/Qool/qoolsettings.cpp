#include "qoolsettings.h"

#include <QCoreApplication>

QOOL_NS_BEGIN

QoolSettings* QoolSettings::m_instance = nullptr;

QoolSettings::QoolSettings()
  : QObject { qApp } {
  m_settings =
    new QSettings(QString("QoolUI"), QString("QoolUI"), this);
}

QoolSettings* QoolSettings::create(QQmlEngine*, QJSEngine*) {
  return instance();
}

void QoolSettings::setValue(const QString& key, const QVariant& value) {
  auto old_value = m_settings->value(key, QVariant());
  if (old_value != value) {
    m_settings->setValue(key, value);
    emit valueChanged(key, value);
  }
}

QVariant QoolSettings::value(
  const QString& key, const QVariant& defaultValue) const {
  return m_settings->value(key, defaultValue);
}

QOOL_NS_END
