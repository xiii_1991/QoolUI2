#include "qool_global.h"
#include "qoolfontmodel.h"
#include "qoolsettings.h"
#include "qoolsingleton.h"

#include <QQmlEngine>
#include <QtQml/qqmlextensionplugin.h>

class QoolExtensionPlugin: public QQmlEngineExtensionPlugin {
  Q_OBJECT
  Q_PLUGIN_METADATA(IID QQmlEngineExtensionInterface_iid)
public:
  void intializeEngine(QQmlEngine* engine, const char* uri) {
    engine->addImportPath(":/qoolui");

#define QOOL_ERI uri, 2, 0

    qmlRegisterSingletonType<QOOL_NS::QoolSingleton>(
      QOOL_ERI, "Qool", QOOL_NS::QoolSingleton::create);

    qmlRegisterSingletonType<QOOL_NS::QoolSettings>(
      QOOL_ERI, "QoolSettings", QOOL_NS::QoolSettings::create);

    qmlRegisterType<QOOL_NS::QoolFontModel>(QOOL_ERI, "QoolFontModel");

#undef QOOL_ERI
  }
};

#include "qoolplugin.moc"
