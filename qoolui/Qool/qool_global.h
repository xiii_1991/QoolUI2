#pragma once

#include <QtCore/qglobal.h>

//#if defined(QOOLUI_LIBRARY)
//  #define QOOLUI_EXPORT Q_DECL_EXPORT
//#else
//  #define QOOLUI_EXPORT Q_DECL_IMPORT
//#endif

#define QOOLUI_EXPORT

#define QOOL_NS qoolui
#define QOOL_NS_BEGIN namespace QOOL_NS {
#define QOOL_NS_END } // qoolui ns

#define QOOL_WRITABLE_PROPERTY(type, name)                             \
protected:                                                             \
  type m_##name;                                                       \
  Q_PROPERTY(                                                          \
    type name READ name WRITE set_##name NOTIFY name##Changed)         \
public:                                                                \
  type name() const { return m_##name; }                               \
  void set_##name(const type& value) {                                 \
    if (value == m_##name)                                             \
      return;                                                          \
    auto old_value = m_##name;                                         \
    m_##name = value;                                                  \
    emit name##Changed(m_##name);                                      \
    emit name##InternalChanged(old_value, m_##name);                   \
  }                                                                    \
  Q_SIGNAL void name##Changed(const type& value);                      \
  Q_SIGNAL void name##InternalChanged(type old, const type& current);

#define QOOL_WRITABLE_PROPERTY_DEF(type, name)                         \
protected:                                                             \
  Q_PROPERTY(                                                          \
    type name READ name WRITE set_##name NOTIFY name##Changed)         \
public:                                                                \
  type name() const;                                                   \
  void set_##name(const type& value);                                  \
  Q_SIGNAL void name##Changed(const type& value);                      \
  Q_SIGNAL void name##InternalChanged(type old, const type& current);

#define QOOL_READONLY_PROPERTY(type, name)                             \
protected:                                                             \
  type m_##name;                                                       \
  Q_PROPERTY(type name READ name NOTIFY name##Changed)                 \
public:                                                                \
  type name() const {                                                  \
    return m_##name;                                                   \
  }                                                                    \
  void update_##name(const type& value) {                              \
    if (value == m_##name)                                             \
      return;                                                          \
    auto old_value = m_##name;                                         \
    m_##name = value;                                                  \
    emit name##Changed(m_##name);                                      \
    emit name##InternalChanged(old_value, m_##name);                   \
  }                                                                    \
  Q_SIGNAL void name##Changed(const type& value);                      \
  Q_SIGNAL void name##InternalChanged(type old, const type& current);

#define QOOL_WRITABLE_OBJ_PROPERTY(type, name)                         \
protected:                                                             \
  type m_##name;                                                       \
  Q_PROPERTY(                                                          \
    type name READ name WRITE set_##name NOTIFY name##Changed)         \
public:                                                                \
  type name() const { return m_##name; }                               \
  void set_##name(type value) {                                        \
    if (value == m_##name)                                             \
      return;                                                          \
    auto old_value = m_##name;                                         \
    m_##name = value;                                                  \
    emit name##Changed(m_##name);                                      \
    emit name##InternalChanged(old_value, m_##name);                   \
  }                                                                    \
  Q_SIGNAL void name##Changed(type value);                             \
  Q_SIGNAL void name##InternalChanged(type old, type current);
