#pragma once

#include "qool_global.h"

#include <QFontDatabase>
#include <QMutex>
#include <QObject>
#include <QQuickItem>

QOOL_NS_BEGIN

class QOOLUI_EXPORT QoolSingleton: public QObject {
  Q_OBJECT
  QML_NAMED_ELEMENT(Qool)
  QML_SINGLETON
  QML_UNCREATABLE("")

protected:
  QoolSingleton();
  static QoolSingleton* m_instance;

public:
  static QoolSingleton* instance() {
    static QMutex mutex;
    if (! m_instance) {
      QMutexLocker locker(&mutex);
      if (! m_instance)
        m_instance = new QoolSingleton;
    }
    return m_instance;
  }

  static QoolSingleton* create(QQmlEngine*, QJSEngine*);

  enum class NumberGateFlag : int {
    NumberGateFix,
    NumberGateCycleFix,
    NumberGateContinuumFix
  };
  Q_ENUM(NumberGateFlag);

  enum class ColorTextMode : int {
    NoColorText,
    HexColorText,
    RGBColorText,
    RGBFColorText
  };
  Q_ENUM(ColorTextMode)

  enum class BoxCornerFlag : int {
    TopLeft,
    TopRight,
    BottomLeft,
    BottomRight
  };
  Q_ENUM(BoxCornerFlag)

  Q_INVOKABLE static QColor bwFromColor(
    const QColor& color, bool preferBlack = false);

  Q_INVOKABLE static QString hexFromColor(
    const QColor& color, bool withAlpha = false);

  Q_INVOKABLE static QStringList rgbaStringFromColor(
    const QColor& color);
  Q_INVOKABLE static QStringList rgbaFloatStringFromColor(
    const QColor& color);

  Q_INVOKABLE static QList<qreal> hsbaFromColor(const QColor& color);

  Q_INVOKABLE static QList<qreal> hslaFromColor(const QColor& color);

  Q_INVOKABLE static QList<qreal> cmykaFromColor(const QColor& color);

  Q_INVOKABLE static QColor colorFromCmyka(
    qreal c, qreal m, qreal y, qreal k, qreal a = 1);

  Q_INVOKABLE static qreal limitNumber(qreal x, qreal min, qreal max);

  Q_INVOKABLE static QStringList fontFamilies(bool allFonts = false);

  Q_INVOKABLE static QString uuid();
};

QOOL_NS_END
