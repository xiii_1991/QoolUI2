.pragma library

function defaultAnimationSwitchFor(item) {
    if (item.parent && item.parent.hasOwnProperty('animationEnabled')) {
        return item.parent.animationEnabled
    } else if (item.window && item.window.hasOwnProperty('animationEnabled')) {
        return item.window.animationEnabled
    }
    return true
}

function defaultAnimationSwitchForWindow(p) {
    if (p && p.hasOwnProperty('animationEnabled')) {
        return p.animationEnabled
    }
    return true
}
