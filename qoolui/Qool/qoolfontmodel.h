#pragma once

#include "qool_global.h"

#include <QAbstractListModel>
#include <QFontDatabase>
#include <QQuickItem>

QOOL_NS_BEGIN

class QOOLUI_EXPORT QoolFontModel: public QAbstractListModel {
  Q_OBJECT
  QML_ELEMENT

protected:
  QStringList m_families;

public:
  explicit QoolFontModel(QObject* parent = nullptr);

  // Header:
  QVariant headerData(int section, Qt::Orientation orientation,
    int role = Qt::DisplayRole) const override;

  // Basic functionality:
  int rowCount(
    const QModelIndex& parent = QModelIndex()) const override;

  QVariant data(const QModelIndex& index,
    int role = Qt::DisplayRole) const override;

private:
};

QOOL_NS_END
