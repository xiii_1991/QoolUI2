import QtQuick

QtObject {
    id: root
    property real top: 1
    property real bottom: 0
    readonly property real distance: Math.abs(top - bottom)


    /*!
      同时检查数值上限和下限
      */
    function fix(x) {
        __checkProperties()
        if (x > top)
            return top
        if (x < bottom)
            return bottom
        return x
    }


    /*!
      只检查数值范围上限。
      */
    function fixTop(x) {
        __checkProperties()
        return Math.min(x, top)
    }


    /*!
      只检查数值范围下限
      */
    function fixBottom(x) {
        __checkProperties()
        return Math.max(x, bottom)
    }


    /*!
      循环检查数值，如果数值低于下限将会从上限继续往下数，反之同理。
      */
    function cycleFix(x) {
        __checkProperties()
        let result = x
        while (result < root.bottom || result > root.top) {
            if (result < root.bottom)
                result += root.distance
            if (result > root.top)
                result -= root.distance
        }
        return result
    }


    /*!
      连续体检查数值，如果超出最大值，立即跳到最小值，反之同理。
      */
    function continuumFix(x) {
        if (x > root.top)
            return root.bottom
        if (x < root.bottom)
            return root.top
        return x
    }

    function __checkProperties() {
        if (top <= bottom)
            console.log(qsTr("NumberGate的最大值小于等于最小值，可能会引发奇怪的结果。"))
    }
}
