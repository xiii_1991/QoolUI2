import QtQuick
import Qool

Item {
    id: root
    anchors.fill: parent
    z: 50
    visible: !parent.enabled

    Rectangle {
        opacity: 0.2
        anchors.fill: parent
        gradient: Gradient {
            orientation: Gradient.Vertical
            stops: [
                GradientStop {
                    position: 0
                    color: "transparent"
                },
                GradientStop {
                    position: 0.35
                    color: "black"
                },
                GradientStop {
                    position: 0.65
                    color: "black"
                },
                GradientStop {
                    position: 1
                    color: "transparent"
                }
            ]
        }
    }

    QoolSmallIndicator {
        text: qsTr("× 已锁定 ×")
        color: QoolStyle.negativeColor
        radius: 3
        anchors.centerIn: parent
    }
}
