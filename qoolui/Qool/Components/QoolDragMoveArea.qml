import QtQuick

MouseArea {
    id: area

    acceptedButtons: Qt.LeftButton
    signal wantToMove(real offsetX, real offsetY)
    readonly property bool hovered: props.hovered

    QtObject {
        id: props
        property bool isMoving: false
        property point startPoint: Qt.point(0, 0)
        property bool hovered: false
    }

    onPressed: {
        props.startPoint = Qt.point(mouseX, mouseY)
        props.isMoving = true
    }

    onPositionChanged: {
        if (props.isMoving) {
            area.wantToMove(mouseX - props.startPoint.x,
                            mouseY - props.startPoint.y)
        }
    }

    onReleased: {
        props.isMoving = false
    }

    onEntered: props.hovered = true
    onExited: props.hovered = false
}
