import QtQuick
import QtQuick.Shapes
import Qool

Shape {
    id: root

    property real strokeWidth: 2
    property color color: "red"
    property color strokeColor: "yellow"
    property real cutSize: width
    property int direction: Qool.TopLeft

    height: width
    containsMode: Shape.FillContains

    QtObject {
        id: privateControl
        readonly property point pointA: root.direction
                                        === Qool.BottomRight ? Qt.point(
                                                                   0,
                                                                   root.cutSize) : Qt.point(
                                                                   0, 0)
        readonly property point pointB: root.direction
                                        === Qool.BottomLeft ? Qt.point(
                                                                  cutSize,
                                                                  cutSize) : Qt.point(
                                                                  cutSize, 0)

        readonly property point pointC: root.direction
                                        === Qool.TopLeft ? Qt.point(
                                                               0,
                                                               cutSize) : Qt.point(
                                                               cutSize, cutSize)
    }

    ShapePath {
        startX: privateControl.pointA.x
        startY: privateControl.pointA.y

        fillColor: root.color
        fillRule: ShapePath.WindingFill
        strokeColor: root.strokeColor
        strokeWidth: root.strokeWidth
        capStyle: ShapePath.RoundCap
        joinStyle: ShapePath.RoundJoin

        PathLine {
            x: privateControl.pointB.x
            y: privateControl.pointB.y
        }

        PathLine {
            x: privateControl.pointC.x
            y: privateControl.pointC.y
        }

        PathLine {
            x: privateControl.pointA.x
            y: privateControl.pointA.y
        }
    }
}
