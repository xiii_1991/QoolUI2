import QtQuick
import QtQuick.Shapes

ShapePath {
    id: root

    property real cutSizeLT: cutSize
    property real cutSizeRT: 0
    property real cutSizeLB: 0
    property real cutSizeRB: 0

    property real width: 100
    property real height: 100

    property real shrinkSize: 0

    strokeWidth: 0
    strokeColor: "transparent"

    startX: cutSizeLT + shrinkSize
    startY: shrinkSize

    PathLine {
        x: width - cutSizeRT - shrinkSize
        y: shrinkSize
    }

    PathLine {
        x: width - shrinkSize
        y: cutSizeRT + shrinkSize
    }

    PathLine {
        x: width - shrinkSize
        y: height - shrinkSize - cutSizeRB
    }

    PathLine {
        x: width - shrinkSize - cutSizeRB
        y: height - shrinkSize
    }

    PathLine {
        x: shrinkSize + cutSizeLB
        y: height - shrinkSize
    }

    PathLine {
        x: shrinkSize
        y: height - shrinkSize - cutSizeLB
    }

    PathLine {
        x: shrinkSize
        y: shrinkSize + cutSizeLT
    }

    PathLine {
        x: startX
        y: startY
    }

    PathMove {
        x: cutSizeLT
        y: 0
    }

    PathLine {
        x: width - cutSizeRT
        y: 0
    }

    PathLine {
        x: width
        y: cutSizeRT
    }

    PathLine {
        x: width
        y: height - cutSizeRB
    }

    PathLine {
        x: width - cutSizeRB
        y: height
    }

    PathLine {
        x: cutSizeLB
        y: height
    }

    PathLine {
        x: 0
        y: height - cutSizeLB
    }

    PathLine {
        x: 0
        y: cutSizeLT
    }

    PathLine {
        x: cutSizeLT
        y: 0
    }
}
