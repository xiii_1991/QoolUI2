import QtQuick
import QtQuick.Shapes

Shape {
    id: root

    property real cutSize: 15
    property real cutSizeLT: cutSize
    property real cutSizeRT: 0
    property real cutSizeLB: 0
    property real cutSizeRB: 0

    property real strokeWidth: 2

    property color color: "red"
    property color strokeColor: "yellow"

    containsMode: Shape.FillContains

    QoolCutCornerStrokeShapePath {
        id: strokeShape
        fillColor: root.strokeColor
        width: root.width
        height: root.height
        shrinkSize: root.strokeWidth
        cutSizeLT: root.cutSizeLT
        cutSizeRT: root.cutSizeRT
        cutSizeLB: root.cutSizeLB
        cutSizeRB: root.cutSizeRB
    }

    QoolCutCornerShapePath {
        id: surfaceShape
        shrinkSize: root.strokeWidth
        fillColor: root.color
        width: root.width
        height: root.height
        cutSizeLT: root.cutSizeLT
        cutSizeRT: root.cutSizeRT
        cutSizeLB: root.cutSizeLB
        cutSizeRB: root.cutSizeRB
    }
}
