import QtQuick
import Qool
import "private"

QoolCutCornerTrianglePrivate {
    id: root
    property bool animationEnabled: QoolFn.defaultAnimationSwitchFor(this)
    Behavior on color {
        enabled: root.animationEnabled
        ColorAnimation {
            duration: QoolStyle.controlTransitionDuration
        }
    }

    Behavior on strokeColor {
        enabled: root.animationEnabled
        ColorAnimation {
            duration: QoolStyle.controlTransitionDuration
        }
    }
    Behavior on cutSize {
        enabled: root.animationEnabled
        NumberAnimation {
            duration: QoolStyle.controlTransitionDuration
        }
    }
    Behavior on strokeWidth {
        enabled: root.animationEnabled
        NumberAnimation {
            duration: QoolStyle.controlTransitionDuration
        }
    }
}
