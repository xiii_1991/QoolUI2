import QtQuick
import Qool

Rectangle {
    id: root
    property string text: qsTr("提示")
    property alias font: textItem.font
    property bool animationEnabled: QoolFn.defaultAnimationSwitchFor(this)

    color: QoolStyle.warningColor
    border.color: QoolStyle.backgroundColor
    border.width: 1
    radius: 3
    implicitHeight: textItem.implicitHeight
    implicitWidth: textItem.implicitWidth

    Text {
        id: textItem
        text: root.text
        font.pixelSize: 8
        color: root.border.color
        font.bold: true
        horizontalAlignment: Qt.AlignHCenter
        verticalAlignment: Qt.AlignVCenter
        leftPadding: 3
        rightPadding: 3
        topPadding: 2
        bottomPadding: 2
        anchors.fill: parent
    }

    Behavior on color {
        enabled: root.animationEnabled
        ColorAnimation {
            duration: QoolStyle.controlMovementDuration
        }
    }
    Behavior on width {
        enabled: root.animationEnabled
        NumberAnimation {
            duration: QoolStyle.controlMovementDuration
        }
    }
    Behavior on opacity {
        enabled: root.animationEnabled
        NumberAnimation {
            duration: QoolStyle.controlMovementDuration
        }
    }
}
