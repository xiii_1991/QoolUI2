import QtQuick
import Qool

Text {
    id: root
    property color bgColor: QoolStyle.infoColor
    property real cutSize: QoolStyle.controlTitleFontPixelSize
    property real spacing: 4

    topPadding: spacing
    leftPadding: Math.max(cutSize, spacing)
    bottomPadding: spacing
    rightPadding: spacing

    width: 200

    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    font.pixelSize: QoolStyle.controlTitleFontPixelSize

    QoolCutCornerBox {
        z: -10
        color: root.bgColor
        strokeColor: root.color
        anchors.fill: parent
        cutSize: root.cutSize
    }
}
