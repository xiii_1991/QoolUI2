import QtQuick
import Qool

Item {
    id: root

    property real shrinkSize: 3
    property real barWidth: 12
    property real barSpace: 12
    property color color: QoolStyle.negativeColor
    property bool animationEnabled: QoolFn.defaultAnimationSwitchFor(this)

    clip: true
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    implicitHeight: parent.height * 0.85
    anchors.margins: shrinkSize
    opacity: parent.enabled ? 0 : 1

    Behavior on color {
        enabled: animationEnabled
        ColorAnimation {
            duration: QoolStyle.controlTransitionDuration
        }
    }
    Behavior on opacity {
        enabled: animationEnabled
        NumberAnimation {
            duration: QoolStyle.controlTransitionDuration
        }
    }

    Component {
        id: bar
        Rectangle {
            id: barRect
            height: root.height * Math.sqrt(2)
            width: barWidth
            border.width: 0
            transform: Rotation {
                angle: 45
            }
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: "transparent"
                }
                GradientStop {
                    position: 1
                    color: Qt.alpha(root.color, 0.5)
                }
            }
        }
    }

    Row {
        id: row
        x: barWidth * 3
        Repeater {
            model: root.width * 2 / row.spacing
            delegate: bar
        }
        spacing: barSpace + barWidth
    }
}
