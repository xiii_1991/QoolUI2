import QtQuick
import "private"
import Qool

QoolCutCornerBoxPrivate {
    id: root
    property bool animationEnabled: QoolFn.defaultAnimationSwitchFor(this)
    Behavior on color {
        enabled: root.animationEnabled
        ColorAnimation {
            duration: QoolStyle.controlTransitionDuration
        }
    }
    Behavior on strokeColor {
        enabled: root.animationEnabled
        ColorAnimation {
            duration: QoolStyle.controlTransitionDuration
        }
    }
    Behavior on cutSizeLT {
        enabled: root.animationEnabled
        NumberAnimation {
            duration: QoolStyle.controlTransitionDuration
        }
    }
    Behavior on cutSizeRT {
        enabled: root.animationEnabled
        NumberAnimation {
            duration: QoolStyle.controlTransitionDuration
        }
    }
    Behavior on cutSizeLB {
        enabled: root.animationEnabled
        NumberAnimation {
            duration: QoolStyle.controlTransitionDuration
        }
    }
    Behavior on cutSizeRB {
        enabled: root.animationEnabled
        NumberAnimation {
            duration: QoolStyle.controlTransitionDuration
        }
    }
    Behavior on strokeWidth {
        enabled: root.animationEnabled
        NumberAnimation {
            duration: QoolStyle.controlTransitionDuration
        }
    }
}
