import QtQuick
import Qool

QoolCutCornerBox {
    id: root

    property color highColor: QoolStyle.highlightColor
    property color lowColor: QoolStyle.backgroundColor

    clip: true

    color: highColor
    strokeColor: lowColor
    strokeWidth: 2
    z: 95
    anchors.fill: parent

    Text {
        id: highlightWord
        z: 1
        color: root.lowColor
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        font.pixelSize: root.height / 2
        font.bold: true
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    function updateHighlightWord() {
        highlightWord.anchors.verticalCenterOffset = Math.random(
                    ) * (root.height / 2) - root.height / 4
        highlightWord.anchors.horizontalCenterOffset = Math.random(
                    ) * (root.width / 2) - root.width / 4
        highlightWord.rotation = -45 + Math.random() * 90
        highlightWord.scale = Math.random() + 1
        highlightWord.text = QoolStyle.papaWords[Math.floor(
                                                     Math.random(
                                                         ) * QoolStyle.papaWords.length)]
    }

    Component.onCompleted: updateHighlightWord()
    onVisibleChanged: if (!visible)
                          updateHighlightWord()
}
