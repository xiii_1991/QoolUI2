import QtQuick
import Qool
import Qool.Controls.Basic

QoolButtonBasic {
    id: root
    property string text
    property color textColor: QoolStyle.textColor
    property color highlightColor: QoolStyle.highlightColor

    cutSize: 5

    checkable: true
    bgColor: highlighted ? root.highlightColor : "transparent"
    strokeColor: containsMouse ? textColor : "transparent"

    implicitWidth: parent.width
    implicitHeight: 25

    contentItem: Text {
        text: root.text
        color: root.highlighted ? QoolStyle.controlBackgroundColor : root.textColor
        horizontalAlignment: Qt.AlignRight
        verticalAlignment: Qt.AlignBottom
    }

    checkingOperation: function () {
        checked = true
    }

    QoolHighlightCover {
        visible: root.down
        cutSize: root.cutSize
    }
}
