import QtQuick

ListModel {
    id: root

    ListElement {
        name: qsTr("欢迎页")
        qml: "pages/PWelcome.qml"
    }

    ListElement {
        name: qsTr("基础按钮")
        qml: "pages/PBasicButtons.qml"
    }

    ListElement {
        name: qsTr("文本控件")
        qml: "pages/PTextInput.qml"
    }

    ListElement {
        name: qsTr("SpinBox")
        qml: "pages/PSpinBox.qml"
    }

    ListElement {
        name: qsTr("ColorInput")
        qml: "pages/PColorInput.qml"
    }
    ListElement {
        name: qsTr("菜单")
        qml: "pages/PMenu.qml"
    }
}
