import QtQuick
import QtQuick.Controls
import Qool.Windows
import Qool.Controls
import Qool.Controls.Inputs
import Qool.Styles

QoolWindow {
    width: 900
    height: 650
    visible: true
    title: qsTr("Hello, Qool World!")

    bgColor: QoolStyle.backgroundColor

    content: SplitView {
        orientation: Qt.Horizontal
        handle: Item {
            implicitWidth: 4
            implicitHeight: 4
        }
        PageSelector {
            id: pageSelector
            SplitView.minimumWidth: 80
            SplitView.preferredWidth: 120
            SplitView.maximumWidth: 200
            onWantToLoadPage: (pageTitle, pageQml) => {
                                  pageLoader.loadPage(pageTitle, pageQml)
                              }
        }
        PageLoader {
            id: pageLoader
        }
    }
}
