//#include "qool_init.h"

#include <QGuiApplication>
#include <QLocale>
#include <QQmlApplicationEngine>
#include <QQmlExtensionPlugin>
#include <QStringLiteral>
#include <QTranslator>

int main(int argc, char* argv[]) {
  QGuiApplication app(argc, argv);

  QTranslator translator;
  const QStringList uiLanguages = QLocale::system().uiLanguages();
  for (const QString &locale : uiLanguages) {
    const QString baseName = "ExampleApp_" + QLocale(locale).name();
    if (translator.load(":/i18n/" + baseName)) {
      app.installTranslator(&translator);
      break;
    }
  }

  //  Q_INIT_RESOURCE(QoolUIResource);
  QQmlApplicationEngine engine;
  //  qoolui_install(&engine);
  //  engine.addImportPath(QStringLiteral("qml"));
  const QUrl url(u"qrc:/QoolUIApp/main.qml"_qs);
  QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
    &app, [url](QObject *obj, const QUrl &objUrl) {
      if (!obj && url == objUrl)
        QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
  engine.load(url);

  return app.exec();
}
