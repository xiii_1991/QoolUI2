import QtQuick
import QtQuick.Controls
import Qool.Controls.Basic
import Qool.Styles

QoolControlBasic {
    id: root

    bgColor: QoolStyle.containerBackgroundColor
    titleFont.pixelSize: 18

    contentItem: ScrollView {
        Loader {
            id: loader
            onLoaded: {
                root.title = loader.item.pageTitle
                root.pageLoaded()
            }
            width: parent.width
        }
    }

    signal pageLoaded

    function loadPage(page) {
        loader.source = page
    }
}
