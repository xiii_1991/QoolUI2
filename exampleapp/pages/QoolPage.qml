import QtQuick

Item {
    id: root

    required property string pageTitle
    required property string pageNote
}
