import QtQuick
import QtQuick.Controls
import Qool.Controls
import Qool.Controls.Inputs
import Qool

QoolPage {
    pageTitle: qsTr("一些文本处理的控件")
    pageNote: qsTr("此处展示了多种处理文本输入的控件")

    Column {
        spacing: 30
        x: 35
        y: 35
        width: 300
        QoolTextField {
            title: qsTr("文本输入控件(单行)")
            width: parent.width
            height: 80
            value: qsTr("白日依山尽，黄河入海流")
        }
    }
}
