import QtQuick
import Qool.Styles

QoolPage {
    pageTitle: qsTr("欢迎使用")
    pageNote: ""
    Column {
        spacing: 20
        Text {
            id: mainTitle
            text: qsTr("QoolUI 控件演示程序")
            color: QoolStyle.textColor
            font.pixelSize: 24
        }

        Text {
            text: qsTr("版本：2.0")
            color: QoolStyle.textColor
        }
    }
}
