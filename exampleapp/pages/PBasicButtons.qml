import QtQuick
import QtQuick.Controls
import Qool.Controls
import Qool
import Qool.Windows

QoolPage {
    id: root
    pageTitle: qsTr("QoolUI 基础按钮演示")
    pageNote: qsTr("基础按钮的功能演示，基础按钮就是基础的按钮。基于这些基础按钮可以很方便地定制更复杂的按钮。")

    Column {
        id: cc
        spacing: 20
        width: 300
        QoolButton {
            text: qsTr("基本的按钮")
            height: 65
            width: 300
            enabled: checkButton.checked
        }

        QoolButton {
            text: qsTr("有标题的按钮")
            title: qsTr("按钮是可以有标题的，没想到吧~")
            showTitle: true
            height: 65
            width: 300
        }

        QoolButton {
            id: checkButton
            text: qsTr("具有激活状态的按钮")
            height: 65
            width: 300
            checkable: true
            title: checked ? qsTr("已激活") : qsTr("未激活")
            showTitle: true
            checked: true
        }

        QoolButton {
            id: customButton
            property color cusColor: QoolStyle.backgroundStrokeColor

            title: qsTr("可以自定义按钮的内容")
            showTitle: true
            height: 65
            width: 300
            contentItem: Item {
                Rectangle {
                    anchors.centerIn: parent
                    height: parent.height
                    width: height
                    color: customButton.cusColor
                    Behavior on color {
                        ColorAnimation {
                            duration: QoolStyle.controlTransitionDuration
                        }
                    }
                    border.color: QoolStyle.backgroundStrokeColor
                    border.width: 3
                    radius: height / 2
                }
            }
        }

        Row {
            spacing: 15
            QoolButton {
                title: qsTr("按钮可以编组")
                showTitle: true
                width: 90
                height: 65
                text: qsTr("A")
                checkable: true
                groupName: "basicbuttontest"
                onClicked: customButton.cusColor = QoolStyle.positiveColor
            }
            QoolButton {
                title: qsTr("按钮可以编组")
                showTitle: true
                width: 90
                height: 65
                text: qsTr("B")
                checkable: true
                groupName: "basicbuttontest"
                onClicked: customButton.cusColor = QoolStyle.negativeColor
            }
            QoolButton {
                title: qsTr("按钮可以编组")
                showTitle: true
                width: 90
                height: 65
                text: qsTr("C")
                checkable: true
                groupName: "basicbuttontest"
                onClicked: customButton.cusColor = QoolStyle.infoColor
            }
        }
    }
}
