import QtQuick
import QtQuick.Controls
import Qool.Controls
import Qool

QoolPage {
    pageTitle: qsTr("菜单功能演示")
    pageNote: qsTr("菜单栏、菜单和菜单控件的演示。菜单控件基于QtQuick的菜单实现，支持Action。")

    QoolMenuBar {
        QoolMenu {
            title: qsTr("菜单1")
            enabled: menuOneSwitch.checked
            Action {
                text: qsTr("打开…")
            }
            Action {
                text: qsTr("保存")
            }
            Action {
                text: qsTr("另存为…")
            }
            Action {
                text: qsTr("关闭")
            }
            QoolMenuSeparator {}
            QoolMenu {
                title: qsTr("分享")
                Action {
                    text: qsTr("到 QQ")
                }
                Action {
                    text: qsTr("到 微博")
                }
                Action {
                    text: qsTr("到 月球")
                }
            }
        }

        QoolMenu {
            title: qsTr("菜单2")
            QoolMenuBanner {
                text: qsTr("QoolUI 的菜单基于 QtQuick.Controls 的菜单控件，所以支持 Action。")
            }
            QoolMenuItem {
                id: menuOneSwitch
                text: qsTr("启用菜单1")
                checkable: true
                checked: true
            }
            QoolMenuItem {
                id: menuThreeSwitch
                text: qsTr("启用菜单3")
                checkable: true
                checked: false
            }
        }

        QoolMenu {
            title: qsTr("菜单3")
            enabled: menuThreeSwitch.checked
            QoolMenuBanner {
                text: qsTr("其实也没什么好介绍的，就是一套主题化的菜单控件而已。")
                color: QoolStyle.tooltipColor
            }
            QoolMenuBanner {
                text: qsTr("以后可能会搞一些更花哨的扩展吧。")
                color: QoolStyle.negativeColor
            }
            Action {
                text: qsTr("关于 QoolUI…")
            }
        }
    } //MenuBar

    QoolCutCornerBox {
        id: clickBox
        width: 100
        height: 100
        cutSize: 5
        color: QoolStyle.infoColor
        strokeColor: QoolStyle.backgroundColor
        cutSizeRB: 5
        x: 200
        y: 150
        Text {
            text: qsTr("右键\n点我")
            anchors.centerIn: parent
            color: Qool.bwFromColor(clickBox.color)
        }

        TapHandler {
            acceptedButtons: Qt.RightButton
            onTapped: ev => {
                          contMenu.popup(ev.x, ev.y)
                      }
        }

        DragHandler {}
    }

    QoolMenu {
        id: contMenu
        title: qsTr("上下文菜单")
        showTitle: acMenuTitle.checked
        QoolMenuBanner {
            text: qsTr("菜单是可以有一个标题栏的，显示的是当前菜单的标题，点击下面的开关试试看？")
        }

        Action {
            id: acMenuTitle
            text: qsTr("启用菜单标题栏")
            checkable: true
            checked: false
        }

        QoolMenuBanner {
            text: qsTr("菜单里面还可以使用Repeater批量添加东西的。")
            color: QoolStyle.tooltipColor
        }

        QoolMenu {
            title: qsTr("选色")
            showTitle: acMenuTitle.checked
            itemHeight: 20
            itemWidth: 45
            Repeater {
                model: 10
                delegate: MenuItem {
                    height: 20
                    readonly property color myColor: Qt.hsva(index / 10, 1, 1,
                                                             1)
                    contentItem: Rectangle {
                        color: myColor
                    }
                    onTriggered: clickBox.color = myColor
                }
            }
        }
    }
}
