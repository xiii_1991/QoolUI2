import QtQuick
import QtQuick.Controls
import Qool.Controls
import Qool.Controls.Inputs
import Qool

QoolPage {
    pageTitle: qsTr("SpinBox 演示")
    pageNote: qsTr("QoolSpinBox 隶属于 QoolInput 工具包，是一个非常方便的 SpinBox。")
    Column {
        id: cc
        spacing: 20
        x: 35
        y: 35
        width: 300
        QoolSpinBox {
            id: spin1
            title: "设置数值"
            unit: "件"
            width: parent.width
            height: 80
            from: 10
            to: 20
            defaultValue: 15
            stepSize: 0.5
            numberFixMode: Qool.NumberGateCycleFix
            editable: false
            QoolToolTip {
                id: spin1tt
                text: qsTr("通过点击 *控件左右侧* 可以进行数值调整。

此控件设置为循环调整溢出量，溢出部分的量将会跳转到另一端边界后并应用。")
            }
        }

        QoolSpinBox {
            id: spin2
            title: qsTr("可使用自定义选项")
            property var options: Array(qsTr("苹果"), qsTr("冬瓜"), qsTr("西瓜"))
            numberFixMode: Qool.NumberGateContinuumFix
            width: parent.width
            height: 80
            from: 0
            to: options.length - 1
            value: 1
            textFromValue: function (x) {
                return options[parseInt(x)]
            }
            editable: false
            stepSize: 1
            QoolToolTip {
                id: spin2tt
                text: qsTr("通过设置数值/文本转换函数，可以呈现多种形式的SpinBox效果。其本质上仍是调整数值的控件。")
            }
        }

        QoolSpinBox {
            id: spin3
            title: qsTr("可编辑的数值")
            unit: qsTr("个盒子")
            width: parent.width
            height: 80
            from: 100
            to: 300
            stepSize: 3.456
            decimals: 3
            editable: true
            enabled: spin2.value === 1
            QoolToolTip {
                id: spin3tt
                text: qsTr("这个控件是可编辑的，可以手动输入数值。")
            }
        }
    } //Column

    Text {
        text: qsTr("QoolSpinBox 是 QoolUI 提供的一个输入控件。

和常见的 SpinBox 一样，它可以用于显示并输入一个数值。数值可设置最大和最小范围，以及每次调整的差值。SpinBox 可以任意设置小数位数，当 *decimals* 设置为 0 时，可以认为是处理整数。数值小数末尾有多余的 0 会被去掉。

数值的展示方式也可以通过设置 *valueFromText* 和 *textFromValue* 两个函数来进行更改，虽然本质上其值仍然是数值类型。

通过设置 *validator* 可以更改数值的验证方式，虽然并不推荐，但是你可以！

还有一个贴心的小设计：通过设置 *unit* 属性，控件可以显示一个小号的文字，用于标识当前值的单位。它还有一个 *unitColor* 属性用于绑定其颜色。事实上，你还可以自己设计 *unitItem* 元素，用于显示自己设计的单位控件，比如更改字体、颜色或使用 Emoji ！

另外，当 *editable* 属性激活时，用户可以通过单击控件中心打开文本框，自己输入想要的数值。顺便说一下，单击控件左侧和右侧则可以用于增加或减少当前数值。

这个控件内置了三种溢出量的处理方法，通过设置 *numberFixMode* 进行切换。它们分别为：

- **不处理** 即数值超过最大值或低于最小值时锁定在边界处
- **循环处理** 溢出边界的量将会在另一端边界处继续叠加
- **跳转处理** 一旦某一侧数值溢出，则会跳转到另一侧的边界处")
        textFormat: Text.MarkdownText
        color: QoolStyle.infoColor
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        anchors {
            left: cc.right
            right: parent.right
            top: parent.top
            margins: 35
        }
    }
}
