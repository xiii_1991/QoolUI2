import QtQuick
import Qool.Controls.Inputs
import Qool

QoolPage {

    pageTitle: qsTr("颜色选择按钮")
    pageNote: qsTr("QoolColorInput 隶属于 QoolInput 工具包，是一个很强大的颜色选择按钮。默认会调用内置的颜色选择对话框。")

    Column {
        id: cc
        spacing: 20
        x: 35
        y: 35
        width: 300
        QoolColorInput {
            title: qsTr("默认外观")
            value: "yellow"
        }
        QoolColorInput {
            title: qsTr("显示色值")
            value: "darkgreen"
            colorTextMode: Qool.HexColorText
        }
        QoolColorInput {
            title: qsTr("显示RGB数值")
            value: "black"
            colorTextMode: Qool.RGBColorText
        }
        QoolColorInput {
            title: qsTr("显示浮点数通道值")
            value: "darkred"
            colorTextMode: Qool.RGBFColorText
            showAlpha: true
        }
        QoolColorInput {
            title: qsTr("禁用状态")
            value: "cyan"
            enabled: false
        }
    }
}
