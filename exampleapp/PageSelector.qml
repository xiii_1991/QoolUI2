import QtQuick
import Qool.Controls
import Qool
import QtQuick.Controls

QoolControl {
    id: root

    signal wantToLoadPage(string pageQml)

    title: qsTr("目录")
    width: 120
    bgColor: QoolStyle.containerBackgroundColor

    contentItem: Column {
        spacing: 5
        Repeater {
            model: TOC {}
            delegate: SelectionButton {
                text: model.name
                groupName: "page_button_group"
                onCheckedChanged: {
                    if (checked)
                        root.wantToLoadPage(model.qml)
                }
                Component.onCompleted: {
                    if (index === 0) {
                        root.wantToLoadPage(model.qml)
                    }
                }
            }
        }
    }
}
